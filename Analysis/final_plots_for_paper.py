import os
import matplotlib as mpl
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
#General script and plotting parameters
script_dir="/home/myoung/Projects/binaryAccretion/Analysis/"
fig_dir="/home/myoung/Projects/binaryAccretion/Paper/Figures/"
data_base_dir="/home/myoung/Output/Binary/"
force_reconvert=False
gxlims=(-2,2)
gylims=(-2,2)
mesh_res=1000
snaps_per_tdyn=10

#Specify the parameters of our simulations
M=1.0
q=.2
a=1.0
j_inf=1.2
execfile(os.path.join(script_dir,"init.py"))

###############################
# Global plotting parameters  #
###############################

#When we zoom in (like for streamlines), what are the limits
zoom_xlim=(-1.6,1)
zoom_ylim=(-1,1)
skew=(zoom_xlim[1]-zoom_xlim[0])/(zoom_ylim[1]-zoom_ylim[0])
zoom_res=(65,50)
#What log ratio of surface density should be displayed
sd_lims=(-2.32,2.32)
#What log ratio of dissipation rates should be displayed
disp_lims=(-4,-0.5)
#What range of forces should we plot
force_lims=(-1,3)
#Default colour map
cmap=plt.cm.Paired
#Colour map for two equally spaced extremes
bcmap=plt.cm.coolwarm
#When plotting lobe overlay, what values of phi should we plot?
bounding_phi=1.*phi1
phis=np.array([phi1,phi2,phi3,bounding_phi])/phi1
#How many snapshots should we smooth across for time variable plots
window=63
window=window+(window%2+1)%2
#Set some font sizes
mpl.rcParams.update({'font.size':30})


###########################
# Lagrange point overview #
###########################

phi_min=-.4
#Do the plotting
fig=plt.figure(figsize=(16.5,13.125))
x=np.linspace(gxlims[0],gxlims[1],mesh_res)
y=np.linspace(gylims[0],gylims[1],mesh_res)
x,y=np.meshgrid(x,y)
phi=dimless_phi(x,y,0,q)
ax=fig.add_subplot(111,aspect='equal')
tmp=ax.pcolormesh(x,y,-np.log10(-phi),vmin=phi_min,cmap=plt.cm.copper)
fig.colorbar(tmp,ax=ax,label="$-\log_{10}(-\Phi)$")
plot_lobe_overlay(ax=ax,labels=True,stars=True,com=True)
ax.set_title("Overview of Roche lobes and Lagrange points",y=1.04)
ax.set_xlabel("x")
ax.set_ylabel("y")
ax.tick_params(pad=10)
fig.tight_layout()
fig.savefig(os.path.join(fig_dir,"Roche_overview.png"))

#################################
# Accretion rates per unit-mass #
#################################

#Cold simulation
Ndots=[1000,200,100]
fig=plt.figure(figsize=(16.5,13.125))
ax=fig.add_subplot(111)
linestyles=['-','--','-.']
for jj,Ntmp in enumerate(Ndots):
  os.chdir(os.path.join(data_base_dir,"2D_cs_.05_j_1.2_q_.2_r_20_Ndot_%d_acc_.01/"%Ntmp))
  execfile(os.path.join(script_dir,"sim_specific_init.py"))
  tmp=np.load("Summary_of_flow.npz")
  #Assume we are starting from zero and going to stop
  accp = tmp['accp']
  accs = tmp['accs']
  #Calculate gamma
  n1=np.diff(accp)
  n2=np.diff(accs)
  n1=np.diff(accp)
  n2=np.diff(accs)
  #Divided by injection rate, where we allow this to vary...
  nt=n1+n2
  qdot=(1+q)*(n2-n1*q)
  sn1=np.nanmean(rolling_window(n1/nt,window),1)
  sn2=np.nanmean(rolling_window(n2/nt,window),1)
  sqdot=np.nanmean(rolling_window(qdot/nt,window),1)
  times=np.arange(window/2,window/2+len(sn1))/snaps_per_tdyn
  ax.plot(times,sn1,label="Primary accretion $\\dot{N}=%d$"%Ntmp,color='blue',linestyle=linestyles[jj])
  ax.plot(times,sn2,label="Secondary accretion $\\dot{N}=%d$"%Ntmp,color='green',linestyle=linestyles[jj])
  ax.plot(times,sqdot,label="$\\dot{q}/\\dot{m}$ $\\dot{N}=%d$"%Ntmp,color='red',linestyle=linestyles[jj])

ax.set_xlabel("# $t_{dyn}$")
ax.set_ylabel("Change per unit mass")
ax.set_title("Accretion rates per unit mass for $c=%g$"%.05,y=1.04)
ax.set_xlim((200,500))
ax.tick_params(pad=10)
fig.tight_layout()
fig.savefig(os.path.join(fig_dir,"Accretion_cold.pdf"))

#Hot simulation
Ndots=[2000,500,200]
fig=plt.figure(figsize=(16.5,13.125))
ax=fig.add_subplot(111)
linestyles=['-','--','-.']
for jj,Ntmp in enumerate(Ndots):
  os.chdir(os.path.join(data_base_dir,"2D_cs_.25_j_1.2_q_.2_r_20_Ndot_%d_acc_.01/"%Ntmp))
  execfile(os.path.join(script_dir,"sim_specific_init.py"))
  tmp=np.load("Summary_of_flow.npz")
  #Assume we are starting from zero and going to stop
  accp = tmp['accp']
  accs = tmp['accs']
  #Calculate gamma
  n1=np.diff(accp)
  n2=np.diff(accs)
  n1=np.diff(accp)
  n2=np.diff(accs)
  #Divided by injection rate, where we allow this to vary...
  nt=n1+n2
  qdot=(1+q)*(n2-n1*q)
  sn1=np.nanmean(rolling_window(n1/nt,window),1)
  sn2=np.nanmean(rolling_window(n2/nt,window),1)
  sqdot=np.nanmean(rolling_window(qdot/nt,window),1)
  times=np.arange(window/2,window/2+len(sn1))/snaps_per_tdyn
  ax.plot(times,sn1,label="Primary accretion $\\dot{N}=%d$"%Ntmp,color='blue',linestyle=linestyles[jj])
  ax.plot(times,sn2,label="Secondary accretion $\\dot{N}=%d$"%Ntmp,color='green',linestyle=linestyles[jj])
  ax.plot(times,sqdot,label="$\\dot{q}/\\dot{m}$ $\\dot{N}=%d$"%Ntmp,color='red',linestyle=linestyles[jj])

ax.set_xlabel("# $t_{dyn}$")
ax.set_ylabel("Change per unit mass")
ax.set_title("Accretion rates per unit mass for $c=%g$"%.25,y=1.04)
ax.set_xlim((200,500))
ax.tick_params(pad=10)
fig.tight_layout()
fig.savefig(os.path.join(fig_dir,"Accretion_hot.pdf"))

#################
# Flow topology #
#################

#Cold plot
phi_cuts=np.array([1,2,5,10])*phi1
R_cuts=np.array([1,2,5,10])*.01
snaps_per_tdyn=10
ss_start=1200
nngbs=19
flow_noload=False
cbdiscR=1.44
alims=(1200,5001)
xlims=(-2,2)
ylims=(-2,2)
highRes=(1200,1200)
lowRes=(200,200)
cs=.05
Ndot=1000
Ndots=[Ndot,500,200]
#Load the main directory
os.chdir(os.path.join(data_base_dir,"2D_cs_.05_j_1.2_q_.2_r_20_Ndot_%d_acc_.01/"%Ndot))
execfile(os.path.join(script_dir,"sim_specific_init.py"))
execfile(os.path.join(script_dir,"averaging.py"))

dat=(avg_rho_high)
res=highRes
x,y=np.meshgrid(np.linspace(xlims[0],xlims[1],res[0]),np.linspace(ylims[0],ylims[1],res[1]))
R=np.sqrt((x+(q/(1.+q)))**2+y**2)
dat_m=np.log10(np.mean(dat[np.logical_and(np.isfinite(dat),R>j_inf**2)]))
dat=np.log10(dat)
dat=dat-dat_m
dat[np.logical_not(np.isfinite(dat))]=0.
fig=plt.figure(figsize=(18.,13.125))
ax=fig.add_subplot(111,aspect='equal')
#tmp=ax.pcolormesh(np.linspace(xlims[0],xlims[1],res[0]),np.linspace(ylims[0],ylims[1],res[1]),dat.T,vmin=dat_m-3*dat_sd,vmax=dat_m+3*dat_sd,cmap=plt.cm.Paired)
dmap=ax.pcolormesh(np.linspace(xlims[0],xlims[1],res[0]),np.linspace(ylims[0],ylims[1],res[1]),dat.T,cmap=cmap,vmin=sd_lims[0],vmax=sd_lims[1],alpha=.1)
ax.set_xlim(zoom_xlim)
ax.set_ylim(zoom_ylim)
dat_U=(avg_U_low)
dat_V=(avg_V_low)
dat=(avg_rho_low)
res=lowRes
x=np.linspace(xlims[0],xlims[1],res[0])
y=np.linspace(ylims[0],ylims[1],res[1])
xx,yy=np.meshgrid(x,y)
R=np.sqrt((xx+(q/(1.+q)))**2+yy**2)
dat_m=np.log10(np.mean(dat[np.logical_and(np.isfinite(dat),R>j_inf**2)]))
dat=np.log10(dat)
dat=dat-dat_m
dat[np.logical_not(np.isfinite(dat))]=0.
ox=((x>zoom_xlim[0]) & (x<zoom_xlim[1]))
oy=((y>zoom_ylim[0]) & (y<zoom_ylim[1]))
x=x[ox]
y=y[oy]
dat_U=dat_U[ox,][:,oy]
dat_V=dat_V[ox,][:,oy]
dat=dat[ox,][:,oy]
xx=xx[ox,][:,oy]
yy=yy[ox,][:,oy]
#fig=plt.figure()
#ax=fig.add_subplot(111)
dat=dat.T
dat[dat<sd_lims[0]]=sd_lims[0]
dat[dat>sd_lims[1]]=sd_lims[1]
dat[0,0]=sd_lims[0]
dat[-1,-1]=sd_lims[1]
#speed=(np.log10(np.sqrt(dat_U**2+dat_V**2))).T
#speed[np.logical_not(np.isfinite(speed))]=0
tmp=ax.streamplot(x,y,
    dat_U.T,
    dat_V.T,
    color=dat,
    linewidth=1,
    arrowsize=2,
    arrowstyle='->',
    cmap=cmap,
    density=4*np.array([skew,1]))
#Make the arrow-heads more visible against background
aa=[x for x in ax.get_children() if type(x)==matplotlib.patches.FancyArrowPatch]
for x in aa:  x.set_edgecolor((0,0,0,1))
ax.set_title("Streamlines of average flow for $c=%g$, $\dot{N}=%d$"%(cs,Ndot),y=1.04)
ax.set_xlabel("x")
ax.set_ylabel("y")
plot_lobe_overlay(ax=ax,labels=True,zero_force=False,pot=phis)
divider = make_axes_locatable(plt.gca())
cax = divider.append_axes("right", "5%", pad="3%")
fig.colorbar(tmp.lines,cax=cax,label="$\\log_{10}(\\Sigma/\\bar{\\Sigma})$",use_gridspec=True)
fig.tight_layout()
fig.savefig(os.path.join(fig_dir,"Streamlines_cold.png"))


#Hot plot
phi_cuts=np.array([1,2,5,10])*phi1
R_cuts=np.array([1,2,5,10])*.01
ss_start=1200
snaps_per_tdyn=10
nngbs=19
flow_noload=False
cbdiscR=1.44
alims=(1200,5001)
xlims=(-2,2)
ylims=(-2,2)
highRes=(1200,1200)
lowRes=(200,200)
cs=.25
Ndot=2000
Ndots=[Ndot,1000,500]
#Load the main directory
os.chdir(os.path.join(data_base_dir,"2D_cs_.25_j_1.2_q_.2_r_20_Ndot_%d_acc_.01/"%Ndot))
execfile(os.path.join(script_dir,"sim_specific_init.py"))
execfile(os.path.join(script_dir,"averaging.py"))

dat=(avg_rho_high)
res=highRes
x,y=np.meshgrid(np.linspace(xlims[0],xlims[1],res[0]),np.linspace(ylims[0],ylims[1],res[1]))
R=np.sqrt((x+(q/(1.+q)))**2+y**2)
dat_m=np.log10(np.mean(dat[np.logical_and(np.isfinite(dat),R>j_inf**2)]))
dat=np.log10(dat)
dat=dat-dat_m
dat[np.logical_not(np.isfinite(dat))]=0.
fig=plt.figure(figsize=(18.,13.125))
ax=fig.add_subplot(111,aspect='equal')
#tmp=ax.pcolormesh(np.linspace(xlims[0],xlims[1],res[0]),np.linspace(ylims[0],ylims[1],res[1]),dat.T,vmin=dat_m-3*dat_sd,vmax=dat_m+3*dat_sd,cmap=plt.cm.Paired)
dmap=ax.pcolormesh(np.linspace(xlims[0],xlims[1],res[0]),np.linspace(ylims[0],ylims[1],res[1]),dat.T,cmap=cmap,vmin=sd_lims[0],vmax=sd_lims[1],alpha=.1)
ax.set_xlim(zoom_xlim)
ax.set_ylim(zoom_ylim)
dat_U=(avg_U_low)
dat_V=(avg_V_low)
dat=(avg_rho_low)
res=lowRes
x=np.linspace(xlims[0],xlims[1],res[0])
y=np.linspace(ylims[0],ylims[1],res[1])
xx,yy=np.meshgrid(x,y)
R=np.sqrt((xx+(q/(1.+q)))**2+yy**2)
dat_m=np.log10(np.mean(dat[np.logical_and(np.isfinite(dat),R>j_inf**2)]))
dat=np.log10(dat)
dat=dat-dat_m
dat[np.logical_not(np.isfinite(dat))]=0.
ox=((x>zoom_xlim[0]) & (x<zoom_xlim[1]))
oy=((y>zoom_ylim[0]) & (y<zoom_ylim[1]))
x=x[ox]
y=y[oy]
dat_U=dat_U[ox,][:,oy]
dat_V=dat_V[ox,][:,oy]
dat=dat[ox,][:,oy]
xx=xx[ox,][:,oy]
yy=yy[ox,][:,oy]
#fig=plt.figure()
#ax=fig.add_subplot(111)
dat=dat.T
dat[dat<sd_lims[0]]=sd_lims[0]
dat[dat>sd_lims[1]]=sd_lims[1]
dat[0,0]=sd_lims[0]
dat[-1,-1]=sd_lims[1]
#speed=(np.log10(np.sqrt(dat_U**2+dat_V**2))).T
#speed[np.logical_not(np.isfinite(speed))]=0
tmp=ax.streamplot(x,y,
    dat_U.T,
    dat_V.T,
    color=dat,
    linewidth=1,
    arrowsize=2,
    arrowstyle='->',
    cmap=cmap,
    density=4*np.array([skew,1]))
#Make the arrow-heads more visible against background
aa=[x for x in ax.get_children() if type(x)==matplotlib.patches.FancyArrowPatch]
for x in aa:  x.set_edgecolor((0,0,0,1))
ax.set_title("Streamlines of average flow for $c=%g$, $\dot{N}=%d$"%(cs,Ndot),y=1.04)
ax.set_xlabel("x")
ax.set_ylabel("y")
plot_lobe_overlay(ax=ax,labels=True,zero_force=False,pot=phis)
divider = make_axes_locatable(plt.gca())
cax = divider.append_axes("right", "5%", pad="3%")
fig.colorbar(tmp.lines,cax=cax,label="$\\log_{10}(\\Sigma/\\bar{\\Sigma})$",use_gridspec=True)
fig.tight_layout()
fig.savefig(os.path.join(fig_dir,"Streamlines_hot.png"))

########################
# Flux across boundary #
########################

#Cold plot
phi_cuts=np.array([1,2,5,10])*phi1
R_cuts=np.array([1,2,5,10])*.01
nngbs=19
snaps_per_tdyn=10
ss_start=1200
flow_noload=False
cbdiscR=1.44
alims=(1200,5001)
xlims=(-2,2)
ylims=(-2,2)
highRes=(1200,1200)
lowRes=(200,200)
cs=.05
Ndot=1000
Ndots=[Ndot,500,200]
#Load the main directory
os.chdir(os.path.join(data_base_dir,"2D_cs_.05_j_1.2_q_.2_r_20_Ndot_%d_acc_.01/"%Ndot))
execfile(os.path.join(script_dir,"sim_specific_init.py"))
execfile(os.path.join(script_dir,"averaging.py"))

ptracer = pflux/(inj_rate*snaps_per_tdyn)
stracer = sflux/(inj_rate*snaps_per_tdyn)
x=lobe1[:,0]
y=lobe1[:,1]
lower=np.min(np.r_[ptracer,stracer])
upper=np.max(np.r_[ptracer,stracer])
lim=max(np.abs(lower),upper)
points = np.array([x,y]).T.reshape(-1,1,2)
segments = np.concatenate([points[:-1], points[1:]], axis=1)
lcp = LineCollection(segments, cmap=plt.get_cmap('coolwarm'),norm=plt.Normalize(-lim,lim))
lcp.set_array(ptracer[np.arange(len(ptracer))])
lcp.set_linewidth(10)
x=lobe2[:,0]
y=lobe2[:,1]
points = np.array([x, y]).T.reshape(-1, 1, 2)
segments = np.concatenate([points[:-1], points[1:]], axis=1)
lcs = LineCollection(segments, cmap=plt.get_cmap('coolwarm'),norm=plt.Normalize(-lim,lim))
lcs.set_array(stracer[np.arange(len(stracer))])
lcs.set_linewidth(10)
#Make a nice roche lobe zoomed in plot
fig=plt.figure(figsize=(20.,13.125))
ax=fig.add_subplot(111,aspect='equal')
ax.set_xlim(zoom_xlim)
ax.set_ylim(zoom_ylim)
plot_lobe_overlay(ax=ax,labels=True,zero_force=False,pot=phis)
ax.add_collection(lcp)
ax.add_collection(lcs)
ax.set_title("Flux through boundary for $c=%g$, $\dot{N}=%d$"%(cs,Ndot),y=1.04)
ax.set_xlabel("x")
ax.set_ylabel("y")
fig.colorbar(lcp,ax=ax,label="Flux/$\\dot{N}$")
fig.tight_layout()
fig.savefig(os.path.join(fig_dir,"Boundary_flux_cold.png"))

#Hot plot
phi_cuts=np.array([1,2,5,10])*phi1
R_cuts=np.array([1,2,5,10])*.01
ss_start=1200
snaps_per_tdyn=10
nngbs=19
flow_noload=False
cbdiscR=1.44
alims=(1200,5001)
xlims=(-2,2)
ylims=(-2,2)
highRes=(1200,1200)
lowRes=(200,200)
cs=.25
Ndot=2000
Ndots=[Ndot,1000,500]
#Load the main directory
os.chdir(os.path.join(data_base_dir,"2D_cs_.25_j_1.2_q_.2_r_20_Ndot_%d_acc_.01/"%Ndot))
execfile(os.path.join(script_dir,"sim_specific_init.py"))
execfile(os.path.join(script_dir,"averaging.py"))

ptracer = pflux/(inj_rate*snaps_per_tdyn)
stracer = sflux/(inj_rate*snaps_per_tdyn)
x=lobe1[:,0]
y=lobe1[:,1]
lower=np.min(np.r_[ptracer,stracer])
upper=np.max(np.r_[ptracer,stracer])
lim=max(np.abs(lower),upper)
points = np.array([x,y]).T.reshape(-1,1,2)
segments = np.concatenate([points[:-1], points[1:]], axis=1)
lcp = LineCollection(segments, cmap=plt.get_cmap('coolwarm'),norm=plt.Normalize(-lim,lim))
lcp.set_array(ptracer[np.arange(len(ptracer))])
lcp.set_linewidth(10)
x=lobe2[:,0]
y=lobe2[:,1]
points = np.array([x, y]).T.reshape(-1, 1, 2)
segments = np.concatenate([points[:-1], points[1:]], axis=1)
lcs = LineCollection(segments, cmap=plt.get_cmap('coolwarm'),norm=plt.Normalize(-lim,lim))
lcs.set_array(stracer[np.arange(len(stracer))])
lcs.set_linewidth(10)
#Make a nice roche lobe zoomed in plot
fig=plt.figure(figsize=(20.,13.125))
ax=fig.add_subplot(111,aspect='equal')
ax.set_xlim(zoom_xlim)
ax.set_ylim(zoom_ylim)
plot_lobe_overlay(ax=ax,labels=True,zero_force=False,pot=phis)
ax.add_collection(lcp)
ax.add_collection(lcs)
ax.set_title("Flux through boundary for $c=%g$, $\dot{N}=%d$"%(cs,Ndot),y=1.04)
ax.set_xlabel("x")
ax.set_ylabel("y")
fig.colorbar(lcp,ax=ax,label="Flux/$\\dot{N}$")
fig.tight_layout()
fig.savefig(os.path.join(fig_dir,"Boundary_flux_hot.png"))



#####################
# Trajectory plot  #
#####################

ntrack=5000
phi_track_cut=2.5*phi1
track_start=4999
track_stop=2000
new_track_stop=4000
R_stop=2.
min_life=300
os.chdir(os.path.join(data_base_dir,"2D_cs_"+str(cs).strip('0')+"_j_1.2_q_.2_r_20_Ndot_%d_acc_.01/")%Ndot)
execfile(os.path.join(script_dir,"sim_specific_init.py"))
execfile(os.path.join(script_dir,"trace.py"))
#Plot an individual particles
pi=6000
#When do we last track this particle?
died=np.where(np.logical_not(np.isnan(results[pi,:,0])))[0][0]
#When was it born? (Remember time decreases with increasing index
born=died+np.sum(np.logical_not(np.isnan(results[pi,:,0])))
#Work out the subset to use
subset=slice(0,100)
#Extract the data
x=results[pi,died:born,0][::-1][subset][::-1]
y=results[pi,died:born,1][::-1][subset][::-1]
J=results[pi,died:born,2][::-1][subset][::-1]
t=np.arange(track_start-died,track_start-born,-1)[::-1][subset][::-1]/snaps_per_tdyn
fig=plt.figure(figsize=(13.125,13.125))
ax1=plt.subplot2grid((5,5),(0,0),colspan=5,rowspan=4,aspect='equal')
ax1.set_xlim(xlims)
ax1.set_ylim(ylims)
ax1.set_xlabel("x")
ax1.set_ylabel("y")
points = np.array([x, y]).T.reshape(-1, 1, 2)
segments = np.concatenate([points[:-1], points[1:]], axis=1)
lc = LineCollection(segments, cmap=plt.get_cmap('Paired'),norm=plt.Normalize(min(t),max(t)))
lc.set_array(t)
lc.set_linewidth(3)
ax1.add_collection(lc)
plot_lobe_overlay(ax=ax1,zero_force=False,labels=True,pot=phis)
ax2=plt.subplot2grid((5,5),(4,0),colspan=5,aspect='equal')
#Reduce number of y-ticks
ax2.locator_params(nbins=4,axis='y')
ax2.plot(t,J)
ax2.set_ylabel("J")
ax2.set_xlabel("$t_{dyn}$")
ax1.set_title("J along particle path",y=1.04)
ax1.tick_params(pad=10)
ax2.tick_params(pad=10)
ax2.set_yticks(ax2.get_yticks())
fig.colorbar(lc,ax=ax1,label="$t_{dyn}$")
fig.tight_layout()
fig.savefig(os.path.join(fig_dir,"Jacobi_trajectory.png"))


##############################
# Resolution dependence plot #
##############################

#Calculate for hot simulations
Ndots=np.array([20,50,100,200,500,1000,2000])
hmeans=np.zeros(len(Ndots))
hstd=np.zeros(len(Ndots))
#h/H=.1 at max and scales with 1/sqrt(N) from there
hhonH = .1/np.sqrt((1.*Ndots)/Ndots[-1])
hhonH = np.array([4.5,2.5,1.5,.75,.45,.3,.2])
for jj,Ndot in enumerate(Ndots):
  print Ndot
  #Load everything
  os.chdir(os.path.join(data_base_dir,"2D_cs_.25_j_1.2_q_.2_r_20_Ndot_%d_acc_.01/"%Ndot))
  tmp=np.load("Summary_of_flow.npz")
  #Assume we are starting from zero and going to stop
  accp = tmp['accp']
  accs = tmp['accs']
  discp = tmp['discp']
  discs = tmp['discs']
  #Calculate gamma
  n1=np.diff(accp)
  n2=np.diff(accs)
  #Divided by injection rate, where we allow this to vary...
  nt=n1+n2
  qdot=(1+q)*(n2-n1*q)/q/nt
  qdot[np.logical_not(np.isfinite(qdot))]=np.nan
  #Average things to plot
  sn1=np.nanmean(rolling_window(n1/nt,window),1)
  sn2=np.nanmean(rolling_window(n2/nt,window),1)
  sqdot=np.nanmean(rolling_window(qdot,window),1)
  times=np.arange(window/2,window/2+len(sn1))/snaps_per_tdyn
  #Get the mean and save it
  hmeans[jj]=np.nanmean(qdot[3740:5000])
  hstd[jj]=np.nanstd(sqdot[3740:5000])


#And for the cold
Ndots=np.array([10,20,50,100,200,500,1000])
cmeans=np.zeros(len(Ndots))
cstd=np.zeros(len(Ndots))
#h/H=.1 at max and scales with 1/sqrt(N) from there
chonH = .1/np.sqrt((1.*Ndots)/Ndots[-1])
chonH = np.array([4.6,2.0,1.0,.25,.1,.06,.04])
for jj,Ndot in enumerate(Ndots):
  print Ndot
  #Load everything
  os.chdir(os.path.join(data_base_dir,"2D_cs_.05_j_1.2_q_.2_r_20_Ndot_%d_acc_.01/"%Ndot))
  tmp=np.load("Summary_of_flow.npz")
  #Assume we are starting from zero and going to stop
  accp = tmp['accp']
  accs = tmp['accs']
  #Calculate gamma
  n1=np.diff(accp)
  n2=np.diff(accs)
  #Divided by injection rate, where we allow this to vary...
  nt=n1+n2
  qdot=(1+q)*(n2-n1*q)/q/nt
  #Average things to plot
  sn1=np.nanmean(rolling_window(n1/nt,window),1)
  sn2=np.nanmean(rolling_window(n2/nt,window),1)
  sqdot=np.nanmean(rolling_window(qdot,window),1)
  times=np.arange(window/2,window/2+len(sn1))/snaps_per_tdyn
  #Get the mean and save it
  cmeans[jj]=np.nanmean(qdot[3740:5000])
  cstd[jj]=np.nanstd(sqdot[3740:5000])


#Finally, plot
fig=plt.figure(figsize=(16.5,13.125))
ax=fig.add_subplot(111)
ax.errorbar(chonH,cmeans,yerr=cstd,fmt='.',label='Cold',markersize=20)
ax.errorbar(hhonH,hmeans,yerr=hstd,fmt='^',label='Hot',markersize=20)
ax.set_xlim((0,5))
ax.plot(ax.get_xlim(),[3.75,3.75],'k--')
ax.set_xlabel("$h/H$")
ax.set_ylabel("$\\Gamma$")
ax.set_title("Resolution dependence of $\\Gamma$",y=1.04)
ax.tick_params(pad=10)
fig.tight_layout()
fig.savefig(os.path.join(fig_dir,"Gamma_resolution.pdf"))



#Determine h/H at edge of disc by eye
os.chdir(os.path.join(data_base_dir,"2D_cs_.05_j_1.2_q_.2_r_20_Ndot_%d_acc_.01/"%Ndot))
plt.figure()
for out in [3500,4000,4500,4999]:
  #Load a snapshot
  mass,pos,npos,vel,nvel,rho,hsml,uint,ids,header,extras,p1,p2 = load_file(out)
  #Plot h/H as a function of position from secondary
  R=np.sqrt((npos[:,0]+1)**2+npos[:,1]**2)[:-2]
  H=R*cs*np.sqrt(M/M2)*np.sqrt(R/a)
  plt.plot(R,hsml/2/H,'.')



