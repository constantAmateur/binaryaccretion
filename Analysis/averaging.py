"""
Parameters:
  bounding_phi - Value of modified potential Phi to use as the boundary
                 for Roche lobes.  Suggest some multiple greater than one
                 of phi1 (value of Phi at L1).  1.2 seems to be a good compromise
  nngbs - How many neighbours should be used for the SPH interpolation of the
          flux on the stellar boundaries.
  flow_noload - If true, we will recalculate everything from scratch rather than
                from the saved values (if they exist).
  cbdiscR - Anything further than this radius from the centre of mass is considered
            part of the circum-binary disc.
  alims - Two elements tupple containing the start/end time over which we should 
          average
  xlims - x limits of the region to average over
  ylims - y limits of the region to average over
  highRes - Two element tupple giving the high resolution grid size (x,y)
  lowRes - Two element tupple giving the low resolution grid size (x,y)
  phi_cuts - Array of cuts for counting particles in nested phi contours.  Must
             all be less than bounding_phi
  R_cuts - Like the above but cuts are in distance from stars.  All R must be less
           than the minimum R for bounding_phi
  ss_start - When does the steady state start (in number of snapshots).  120 dynamical times should be ample for standard parameters.
"""

#################################
# Define boundaries and normals #
#################################

#Define boundary, lengths and surface normal
lobe1,lobe2=build_boundary(primary_boundary_by_phi,secondary_boundary_by_phi,phi_max=bounding_phi)
lobes=np.r_[lobe1,lobe2]
#To make this more general, should be able to take a general function which will calculate the unit normal's at each location in lobes.  For now assume grad_phi gives the normal
gx,gy,gz = dimless_grad_phi(lobes[:,0],lobes[:,1],0,q)
tmp=np.sqrt(gx**2+gy**2+gz**2)
gx=gx/tmp
gy=gy/tmp
gz=gz/tmp
gs=np.c_[gx,gy,gz]
#Calculate extra lobe parameters
l1_phi = np.arctan2(lobe1[:,1],lobe1[:,0])
l2_phi = np.arctan2(lobe2[:,1],lobe2[:,0]+1)
#The distance between adjacent points (wrapping around from last to first)
l1_len = np.r_[np.sqrt(np.diff(lobe1[:,0])**2+np.diff(lobe1[:,1])**2),np.sqrt((lobe1[0,0]-lobe1[-1,0])**2+(lobe1[0,1]-lobe1[-1,1])**2)]
l2_len = np.r_[np.sqrt(np.diff(lobe2[:,0])**2+np.diff(lobe2[:,1])**2),np.sqrt((lobe2[0,0]-lobe2[-1,0])**2+(lobe2[0,1]-lobe2[-1,1])**2)]
#Set the length at each point to be the average of adjacent lengths
l1_len = 0.5*(l1_len+np.r_[l1_len[-1],l1_len[:-1]])
l2_len = 0.5*(l2_len+np.r_[l2_len[-1],l2_len[:-1]])


##########################
# Resume from saved file #
##########################

#Can we load and continue?
try:
  #Should we restart?
  if flow_noload:
    raise IOError
  #This will only work if we have the same boundary criteria
  tmp=np.load("Summary_of_flow.npz")
  #extract the pcle dict and ii
  ii=tmp['ii'].item()
  pcle_dict = tmp['pcle_dict'].item()
  #Assume we are starting from zero and going to stop
  accp = tmp['accp']
  accs = tmp['accs']
  discp = tmp['discp']
  discs = tmp['discs']
  discb = tmp['discb']
  infall = tmp['infall']
  flux = tmp['flux']
  #Load the averaging things
  high_cnts=tmp['high_cnts']
  low_cnts=tmp['low_cnts']
  high_Us=tmp['high_Us']
  high_Vs=tmp['high_Vs']
  low_Us=tmp['low_Us']
  low_Vs=tmp['low_Vs']
  high_rhos=tmp['high_rhos']
  low_rhos=tmp['low_rhos']
  high_disps=tmp['high_disps']
  low_disps=tmp['low_disps']
  nested_phi_pdiscs=tmp['nested_phi_pdiscs']
  nested_phi_sdiscs=tmp['nested_phi_sdiscs']
  nested_R_pdiscs=tmp['nested_R_pdiscs']
  nested_R_sdiscs=tmp['nested_R_sdiscs']
  counts=tmp['counts']
  #Check that we're using the same parameters
  for param in ['bounding_phi','phi_cuts','R_cuts','nngbs','cbdiscR','alims','xlims','ylims','highRes','lowRes']:
    sval=tmp[param]
    rval=eval(param)
    passed=1
    try:
      for i in xrange(len(rval)):
        if rval[i]!=sval[i]:
          passed=0
    except TypeError:
      if rval!=sval:
        passed=0
    if passed==0:
      raise ValueError("Stored parameters don't match those given.  Restarting...")
  #If the stored one is bigger, do nothing...
  if accp.shape[0]>(stats_stop-stats_start):
    raise ValueError("Stored flow properties cover greater range than currently accessible")
  #Extend to store new values
  start=accp.shape[0]
  accp = np.r_[accp,np.zeros(stats_stop-start)]
  accs = np.r_[accs,np.zeros(stats_stop-start)]
  discp = np.r_[discp,np.zeros(stats_stop-start)]
  discs = np.r_[discs,np.zeros(stats_stop-start)]
  discb = np.r_[discb,np.zeros(stats_stop-start)]
  infall = np.r_[infall,np.zeros(stats_stop-start)]
  if flux.shape[0]!=len(lobes):
    raise ValueError("Stored lobe size does not match current one.  Can only continue if lobe sizes match")
  flux = np.c_[flux,np.zeros((len(lobes),(stats_stop-start)))]
  nested_phi_pdiscs=np.r_[nested_phi_pdiscs,np.zeros((stats_stop-start,len(phi_cuts)))]
  nested_phi_sdiscs=np.r_[nested_phi_sdiscs,np.zeros((stats_stop-start,len(phi_cuts)))]
  nested_R_pdiscs=np.r_[nested_R_pdiscs,np.zeros((stats_stop-start,len(R_cuts)))]
  nested_R_sdiscs=np.r_[nested_R_sdiscs,np.zeros((stats_stop-start,len(R_cuts)))]
  rphi=[0,0]
except IOError:
  #Starting from scratch...
  ii=0
  accp = np.zeros(stats_stop-stats_start)
  accs = np.zeros(stats_stop-stats_start)
  discp = np.zeros(stats_stop-stats_start)
  discs = np.zeros(stats_stop-stats_start)
  discb = np.zeros(stats_stop-stats_start)
  infall = np.zeros(stats_stop-stats_start)
  pcle_dict = {}
  flux=np.zeros((len(lobes),stats_stop-stats_start))
  #Set up the averaging variables
  high_cnts=np.zeros(highRes)
  low_cnts=np.zeros(lowRes)
  high_Us=np.zeros(highRes)
  high_Vs=np.zeros(highRes)
  low_Us=np.zeros(lowRes)
  low_Vs=np.zeros(lowRes)
  high_rhos=np.zeros(highRes)
  low_rhos=np.zeros(lowRes)
  high_disps=np.zeros(highRes)
  low_disps=np.zeros(lowRes)
  nested_phi_pdiscs=np.zeros((stats_stop-stats_start,len(phi_cuts)))
  nested_phi_sdiscs=np.zeros((stats_stop-stats_start,len(phi_cuts)))
  nested_R_pdiscs=np.zeros((stats_stop-stats_start,len(R_cuts)))
  nested_R_sdiscs=np.zeros((stats_stop-stats_start,len(R_cuts)))
  counts=0
  rphi=[0,0]



############################
# Core loop over snapshots #
############################

#for ii in xrange(stats_start,stats_stop):
while ii<stats_stop:
  print "Loading snapshot %d"%ii
  #Load and conversion
  mass,pos,npos,vel,nvel,rho,hsml,uint,ids,header,extras,p1,p2 = load_file(ii)

  #########################
  # Averaging (if needed) #
  #########################
  if ii>=alims[0] and ii<=alims[1]:
    #Limit ourselves to the particles in the region we care about (and drop the stars, assuming they're in there)
    o=np.where((npos[:,0]<xlims[1]) & (npos[:,0]>xlims[0]) & (npos[:,1]<ylims[1]) & (npos[:,1]>ylims[0]))[0][:-2]
    #What are the high and low res indices for each of these within this domain
    high_xx=((npos[o,0]-xlims[0])*(highRes[0])/(xlims[1]-xlims[0])).astype(int)
    high_yy=((npos[o,1]-ylims[0])*(highRes[1])/(ylims[1]-ylims[0])).astype(int)
    low_xx=((npos[o,0]-xlims[0])*(lowRes[0])/(xlims[1]-xlims[0])).astype(int)
    low_yy=((npos[o,1]-ylims[0])*(lowRes[1])/(ylims[1]-ylims[0])).astype(int)
    #How many snapshots have we averaged over
    counts=counts+1.
    for k in xrange(len(o)):
      high_cnts[high_xx[k],high_yy[k]] += 1.
      low_cnts[low_xx[k],low_yy[k]] += 1.
      high_Us[high_xx[k],high_yy[k]] += nvel[o[k],0]
      high_Vs[high_xx[k],high_yy[k]] += nvel[o[k],1]
      low_Us[low_xx[k],low_yy[k]] += nvel[o[k],0]
      low_Vs[low_xx[k],low_yy[k]] += nvel[o[k],1]
      high_rhos[high_xx[k],high_yy[k]] += rho[o[k]]
      low_rhos[low_xx[k],low_yy[k]] += rho[o[k]]
      high_disps[high_xx[k],high_yy[k]] += extras[0][o[k]]
      low_disps[low_xx[k],low_yy[k]] += extras[0][o[k]]

  ################################
  # Calculate standard quantities#
  ################################
  #Calculate Jacobi constant
  J=-M2/np.sqrt((npos[:-2,0]+1)**2+npos[:-2,1]**2+npos[:-2,2]**2)-M1/np.sqrt(npos[:-2,0]**2+npos[:-2,1]**2+npos[:-2,2]**2)-0.5*omega*omega*((npos[:-2,0]+(a*q/(1+q)))**2+npos[:-2,1]**2)+0.5*(nvel[:-2,0]**2+nvel[:-2,1]**2+nvel[:-2,2]**2)+uint*np.log(rho)
  #Calculate phi
  phi = dimless_phi(npos[:,0],npos[:,1],0,q)
  #Calculate distance from COM
  R = np.sqrt((npos[:,0]+(q/(1.+q)))**2+npos[:,1]**2)
  #Calculate the marker
  markers = np.zeros(phi.shape,dtype=int)
  markers[primary_boundary_by_phi(phi,npos[:,0],npos[:,1],phi_max=bounding_phi)]=1
  markers[secondary_boundary_by_phi(phi,npos[:,0],npos[:,1],phi_max=bounding_phi)]=2
  ###############################
  # Membership interrogation ####
  ###############################
  #What are the masses of the primary/secondary/other?
  discp[ii]=np.sum(markers==1)
  discs[ii]=np.sum(markers==2)
  discb[ii]=np.sum(R[markers==0]<=cbdiscR)
  accp[ii]=stint.unpack(stfloat.pack(extras[-1][p1]))[0]
  accs[ii]=stint.unpack(stfloat.pack(extras[-1][p2]))[0]
  infall[ii]=np.sum(R>cbdiscR)
  Rp=np.sqrt(npos[:,0]**2+npos[:,1]**2)
  Rs=np.sqrt((npos[:,0]+1)**2+npos[:,1]**2)
  for i in xrange(len(phi_cuts)):
    nested_phi_pdiscs[ii,i]=np.sum((markers==1) & (phi<=phi_cuts[i]))
    nested_phi_sdiscs[ii,i]=np.sum((markers==2) & (phi<=phi_cuts[i]))
  for i in xrange(len(R_cuts)):
    nested_R_pdiscs[ii,i]=np.sum((markers==1) & (Rp<=R_cuts[i]))
    nested_R_sdiscs[ii,i]=np.sum((markers==2) & (Rs<=R_cuts[i]))
  #Build the tree
  pt_tree=cKDTree(npos[:-2,:2])
  ##Get phi,dphi for everything
  #phi = dimless_phi(npos[:-2,0],npos[:-2,1],npos[:-2,2],q)
  #dphi_x,dphi_y,dphi_z = dimless_grad_phi(npos[:-2,0],npos[:-2,1],npos[:-2,2],q)
  #dphi=np.sqrt(dphi_x**2+dphi_y**2+dphi_z**2)
  ##Subset of particles that can potentially reach the critical value of phi
  #o=np.where((phi1>(phi-hsml*dphi)) & (phi1<(phi+hsml*dphi)))[0]
  #Do the rough "constant neighbours" style interpolation
  lobeh=np.zeros(len(lobes))
  for j in xrange(len(lobes)):
    rs,pts=pt_tree.query(lobes[j],nngbs)
    lobeh[j]=np.max(rs)*0.5
    qs=rs/lobeh[j]
    weights=W(qs,lobeh[j],ndim=2)
    flux[j][ii]=np.sum(weights*np.sum(gs[j,]*nvel[pts,],1))
  #Add lobe members to dictionary
  o=np.where(markers!=0)[0]
  #Make list of those in marked regions previously
  pids=[x for x in pcle_dict.keys() if pcle_dict[x][-1][0]>0]
  #Calculate the angle of current members
  rphi[0]=np.arctan2(npos[o,1],npos[o,0])
  rphi[1]=np.arctan2(npos[o,1],npos[o,0]+1.0)
  #Loop over all current members and store any changes
  for ctr,j in enumerate(o):
    #It's completely new, so add it
    if ids[j] not in pcle_dict:
      pcle_dict[ids[j]] = [(markers[j],rphi[markers[j]-1][ctr],ii)]
    #It's not completely new, but has it changed since last time?
    elif pcle_dict[ids[j]][-1][0] != markers[j]:
      #Is it a direct swap?  That needs special attention
      prev=pcle_dict[ids[j]][-1][0]
      if prev>0:
        #First leave the old region
        pcle_dict[ids[j]].append((-prev,rphi[prev-1][ctr],ii))
      #Now we can enter the new region
      pcle_dict[ids[j]].append((markers[j],rphi[markers[j]-1][ctr],ii))
  #Take the list of those in marked regions previously and find out where they are now
  o=match(pids,ids)
  o=o[np.isfinite(o)].astype(int)
  #If they're still in a marked region, they were updated by the for loop above, but if they're
  #not, we need to update their leaving status
  o=o[markers[o]==0]
  rphi[0]=np.arctan2(npos[o,1],npos[o,0])
  rphi[1]=np.arctan2(npos[o,1],npos[o,0]+1.0)
  for ctr,j in enumerate(o):
    prev=pcle_dict[ids[j]][-1][0]
    pcle_dict[ids[j]].append((-prev,rphi[prev-1][ctr],ii))
  #Iterate counter
  ii=ii+1

#If we want to save the result for later resuming, we need to save:
#For tracking fluxes
#pcle_dict,discp,discs,discb,accp,accs,infall,flux,ii
#For averaging
#high_cnts,low_cnts,high_Us,high_Vs,low_Us,low_Vs,high_rhos,low_rhos,high_disps,low_disps,counts

#Make a backup copy if it exists
try:
  shutil.copy("Summary_of_flow.npz","Summary_of_flow.npz.bak")
except IOError:
  pass
#Save new one
np.savez_compressed("Summary_of_flow.npz",
    pcle_dict=pcle_dict,
    discp=discp,
    discs=discs,
    discb=discb,
    accp=accp,
    accs=accs,
    infall=infall,
    flux=flux,
    ii=ii,
    high_cnts=high_cnts,
    low_cnts=low_cnts,
    high_Us=high_Us,
    high_Vs=high_Vs,
    low_Us=low_Us,
    low_Vs=low_Vs,
    high_rhos=high_rhos,
    low_rhos=low_rhos,
    high_disps=high_disps,
    low_disps=low_disps,
    counts=counts,
    bounding_phi=bounding_phi,
    phi_cuts=phi_cuts,
    R_cuts=R_cuts,
    nngbs=nngbs,
    cbdiscR=cbdiscR,
    alims=alims,
    xlims=xlims,
    ylims=ylims,
    highRes=highRes,
    lowRes=lowRes,
    nested_phi_pdiscs=nested_phi_pdiscs,
    nested_phi_sdiscs=nested_phi_sdiscs,
    nested_R_pdiscs=nested_R_pdiscs,
    nested_R_sdiscs=nested_R_sdiscs,
    )
#######################
# Finalise properties #
#######################


#We have sums, convert to averages
avg_cnt_high=high_cnts/counts
avg_cnt_low=low_cnts/counts
avg_U_high=high_Us/high_cnts
avg_V_high=high_Vs/high_cnts
avg_U_low=low_Us/low_cnts
avg_V_low=low_Vs/low_cnts
avg_rho_high=high_rhos/high_cnts
avg_rho_low=low_rhos/low_cnts
avg_disp_high=high_disps/high_cnts
avg_disp_low=low_disps/low_cnts
#What should we do with the empty cells?
avg_U_high[np.isnan(avg_U_high)]=0.
avg_V_high[np.isnan(avg_V_high)]=0.
avg_U_low[np.isnan(avg_U_low)]=0.
avg_V_low[np.isnan(avg_V_low)]=0.
avg_rho_high[np.isnan(avg_rho_high)]=0.
avg_rho_low[np.isnan(avg_rho_low)]=0.
avg_disp_high[np.isnan(avg_disp_high)]=0.
avg_disp_low[np.isnan(avg_disp_low)]=0.

#Calculating fluxes
f1=flux[:len(lobe1),:]
f2=flux[len(lobe1):,:]
pflux=np.median(f1[:,ss_start:],1)
sflux=np.median(f2[:,ss_start:],1)
#Number on primary
allp=(discp+accp)
alls=(discs+accs)
#Total number of particles 
total=discp+discs+accp+accs+discb+infall
#Estimate injection rate
_,y=smoothed_gradient(total,window=63)
#Only over first few t_dyn in case particles spill back out
inj_rate=np.median(y[:ss_start])
#Calculate the total flux onto primary and secondary
tp=np.sum(np.outer(l1_len,np.ones(f1.shape[1]))*f1,0)
#tp=np.median(rolling_window(tp,window),1)
ts=np.sum(np.outer(l2_len,np.ones(f2.shape[1]))*f2,0)
#ts=np.median(rolling_window(ts,window),1)
#Estimate gradient from particle numbers
subx,M1dot=smoothed_gradient(np.arange(stats_start,stats_stop),allp,window=window)
subx,M2dot=smoothed_gradient(np.arange(stats_start,stats_stop),alls,window=window)
M1dot=M1dot/inj_rate
M2dot=M2dot/inj_rate
#Calculate dqdt
mass,pos,npos,vel,nvel,rho,hsml,uint,ids,header,extras,p1,p2 = load_file(0)
m=mass[0]
#Minus sign because tp/ts are negative for inflow and 10 because tp/ts are per snapshot not per dynamical time
dqdt=-q*((m*ts/M2/snaps_per_tdyn)-(m*tp/snaps_per_tdyn/M1))
#This is qdot/mdot, where mdot is mass injection rate
norm_qdot=-((q+1)/M)*(ts-q*tp)/(inj_rate*snaps_per_tdyn)

#Construct fake accretion boundary at phi limit
pcnt=np.zeros(len(accp))
scnt=np.zeros(len(accp))
for k in pcle_dict.keys():
  if pcle_dict[k][0][0]==1:
    pcnt[pcle_dict[k][0][2]:] += 1
  else:
    scnt[pcle_dict[k][0][2]:] += 1


