"""
This file will create all the useful functions for analysing binary output, 
tailored to the binary parameters assumed to be in the namespace already 
(M,q,a,j_inf,cs).  Also pre-calculates boundaries of Roche lobes, the limits of the lobes and location of Lagrange points.
"""

from scipy.spatial import cKDTree
from scipy.interpolate import UnivariateSpline
import scipy.ndimage as ndi
import os
import numpy as np
import shutil
import struct as st
from matplotlib.collections import LineCollection
from matplotlib import pyplot as plt

#Infer parameters for the simulation...
M1 = M/(1+q)
M2 = q*M1
M=M1+M2
omega = np.sqrt(M/a**3)


#Functions that we'd usually load from mypy
def match(a,b):
  """Returns indicies where a matches b or nan if they don't.  Same as match in R"""
  ret=np.empty(len(a))
  d=dict([[b[i],i] for i in xrange(len(b))])
  return np.array([d[j] if j in d else np.nan for j in a])

def W(q,h,ndim=3):
  """A SPH smoothing function.  Here q=|r_a-r_b|/h"""
  if isinstance(q,int) or isinstance(q,float):
    q=np.array([q])
  if ndim==1:
    sigma=2./3.
  elif ndim==2:
    sigma=10./(7.*np.pi)
  elif ndim==3:
    sigma=1./np.pi
  else:
    raise NameError('ndim must be between 1-3')
  out=q.copy()
  c1=(q>=0) & (q<1)
  c2=(q>=1) & (q<2)
  c3=(q>=2)
  out[c1] = (sigma/(h**ndim))*(1.-(1.5)*q[c1]**2+(.75)*q[c1]**3)
  out[c2] = (sigma/(h**ndim))*(.25*(2-q[c2])**3)
  out[c3] = 0
  return out

def fillArray(dat,method,N=1,M=1):
  """
  Use this function for reading and decoding large binary blocks
  to avoid memory issues associated with using struct directly
  """
  N=int(N)
  M=int(M)
  sz=method.size
  dtype=float
  if method.format.lower()=='i':
    dtype=int
  if N==1:
    out=method.unpack(dat)[0]
  else:
    if M==1:
      out=np.empty(N,dtype=dtype)
      for i in xrange(N):
        out[i]=method.unpack(dat[(i*sz):((i+1)*sz)])[0]
    else:
      out=np.empty((N,M),dtype=dtype)
      for i in xrange(N*M):
        out[i/M][i%M] = method.unpack(dat[(i*sz):((i+1)*sz)])[0]
  return out

def read_gadget_binary(filename,extras=False,subset=None):
  """
  Given a gadget binary with name "filename", reads it into a series of arrays.
  Returns a tuple containing (filename,masses,pos,vel,rho,hsml,uint,ids,header)
  If extras is true, we will try and return the extra quantities in an array
  If subset is a list of attributes, only these attributes will be loaded.  The
  return tupple isn't changed, but unloaded entries are set to None.  The list 
  of attributes is as above, that is: masses,pos,vel,rho,hsml,uint,ids,extras
  The header is always loaded.
  """
  #Create all the unpacking methods we need
  stint=st.Struct('i')
  stuint=st.Struct("I")
  stfloat=st.Struct('f')
  stdouble=st.Struct('d')
  #Open the file
  with open(filename,'rb') as f:
    #How long is the file?
    f.seek(0,2)
    flen = f.tell()
    #Skip the bytes that tell us the block length
    f.seek(4,0)
    #Read the header
    header={}
    header['npart']=fillArray(f.read(24),stint,6)
    header['mass']=fillArray(f.read(48),stdouble,6)
    header['time']=fillArray(f.read(8),stdouble)
    header['redshift']=fillArray(f.read(8),stdouble)
    header['flag_sfr']=fillArray(f.read(4),stint)
    header['flag_feedback']=fillArray(f.read(4),stint)
    header['nall']=fillArray(f.read(24),stuint,6)
    header['flag_cooling']=fillArray(f.read(4),stint)
    header['numfiles']=fillArray(f.read(4),stint)
    header['boxsize']=fillArray(f.read(8),stdouble)
    header['omega0']=fillArray(f.read(8),stdouble)
    header['omega_lambda']=fillArray(f.read(8),stdouble)
    header['hubble_param']=fillArray(f.read(8),stdouble)
    header['flag_metals']=fillArray(f.read(4),stint)
    header['nallhw']=fillArray(f.read(24),stuint,6)
    header['flag_entr_ics']=fillArray(f.read(4),stint)
    header['extra_output']=fillArray(f.read(4),stuint)
    #Skip to start of positions 256 for block 12 for 3 headers
    f.seek(268)
    N=np.sum(header['npart'])
    n=header['npart'][0]
    #Now read in positions
    if subset is None or 'pos' in subset:
      pos = fillArray(f.read(N*3*4),stfloat,N,3)
    else:
      pos = None
      f.seek(N*3*4,1)
    #Skip the useless headers
    f.seek(8,1)
    if subset is None or 'vel' in subset:
      vel = fillArray(f.read(N*3*4),stfloat,N,3)
    else:
      vel = None
      f.seek(N*3*4,1)
    f.seek(8,1)
    #IDs
    if subset is None or 'ids' in subset:
      ids = fillArray(f.read(N*4),stuint,N)
    else:
      ids = None
      f.seek(N*4,1)
    f.seek(8,1)
    #Mass, if it's needed
    Nm = np.sum(header['npart']*(header['mass']==0))
    if subset is None or 'masses' in subset:
      masses = np.repeat(header['mass'],header['npart'])
      if Nm:
        mtmp = fillArray(f.read(Nm*4),stfloat,Nm)
        masses[masses==0] = mtmp
        f.seek(8,1)
    else:
      masses=None
      if Nm:
        f.seek(Nm*4,1)
        f.seek(8,1)
    #Now the SPH properties, if they're there
    uint=None
    rho=None
    hsml=None
    if n:
      #Internal energy
      if subset is None or 'uint' in subset:
        uint = fillArray(f.read(n*4),stfloat,n)
      else:
        uint = None
        f.seek(n*4,1)
      f.seek(8,1)
      #density if it's there
      if f.tell()+n*4 < flen:
        if subset is None or 'rho' in subset:
          rho = fillArray(f.read(n*4),stfloat,n)
        else:
          rho = None
          f.seek(n*4,1)
        f.seek(8,1)
      #Smoothing length, if it's there
      if f.tell()+n*4 < flen:
        if subset is None or 'hsml' in subset:
          hsml = fillArray(f.read(n*4),stfloat,n)
        else:
          hsml = None
          f.seek(n*4,1)
        f.seek(8,1)
    #Any extra quantities to calculate?
    bonus=[]
    if extras and f.tell()!=flen and (subset is None or 'extras' in subset):
      f.seek(-4,1)
      while(f.tell()!=flen):
        #How long is block?
        blen=st.unpack('i',f.read(4))[0]
        #Is it an "all type" property?
        if blen%N==0:
          #Is it an array, or a single value?
          if blen/(4*N)==1:
            bonus.append(fillArray(f.read(blen),stfloat,N))
          else:
            bonus.append(fillArray(f.read(blen),stfloat,N,blen/(4*N)))
        elif blen%n==0:
          #Array or single value per particle?
          if blen/(4*n)==1:
            bonus.append(fillArray(f.read(blen),stfloat,n))
          else:
            bonus.append(fillArray(f.read(blen),stfloat,n,blen/(4*n)))
        else:
          print "WARNING: Unknown block at end of array, has size %d which is enough space for %d floats (n=%d,N=%d)."%(blen,blen/4.0,n,N)
          f.seek(blen,1)
        #Skip to next 
        f.seek(4,1)
    #Finished core file, ignore any extra data...
    f.close()
  #Return the data
  if extras:
    return (filename,masses,pos,vel,rho,hsml,uint,ids,header,bonus)
  return (filename,masses,pos,vel,rho,hsml,uint,ids,header)



def write_gadget_binary(filename,mass,pos,vel,rho=None,hsml=None,int_energy=None,ids=None,header=None,extras=None):
  """
  Given a set of arrays containing mass, postition, velocity and internal energy,
  writes out a GADGET2 format binary.  N is the total number of particles and n
  is the number of gas particles (SPH particles).
  pos and vel must be Nx3 arrays.
  mass can be either a single float, or an array of length N.
  int_energy must be an array of length n (number of gas particles).
  ids is an array of length N giving the ids of each particle.  Auto-generated if set to None.
  header is a dictionary containing values with which to fill the header block (see GADGET user-guide).
  rho is an array of length n.  Can be omitted
  hsml (SPH smoothing length) is an array of length n.  Can be omitted
  """
  #Make sure the header dictionary has all the information to build the GADGET header
  if header is None:
    header={}
  if type(mass)==float or len(mass)!=pos.shape[0]:
    mass=np.repeat(mass,pos.shape[0])
  if 'time' not in header:
    header['time']=0.0
  if 'redshift' not in header:
    header['redshift']=0.0
  if 'flag_sfr' not in header:
    header['flag_sfr']=0
  if 'flag_feedback' not in header:
    header['flag_feedback']=0
  if 'flag_cooling' not in header:
    header['flag_cooling']=0
  if 'boxsize' not in header:
    header['boxsize']=0
  if 'omega0' not in header:
    header['omega0']=0
  if 'omega_lambda' not in header:
    header['omega_lambda']=0
  if 'hubble_param' not in header:
    header['hubble_param']=0
  if 'flag_age' not in header:
    header['flag_age']=0
  if 'flag_metals' not in header:
    header['flag_metals']=0
  if 'flag_entr_ics' not in header:
    header['flag_entr_ics']=0
  #Assume that there are only gas and sink particles
  if 'npart' not in header:
    header['npart']=np.array([len(int_energy),pos.shape[0]-len(int_energy),0,0,0,0])
  if 'mass' not in header:
    header['mass']=np.zeros(6)
  if 'nall' not in header:
    header['nall']=header['npart']
  if 'numfiles' not in header:
    header['numfiles']=1
  if 'nallhw' not in header:
    header['nallhw']=np.zeros(6)
  if 'extra_output' not in header:
    header['extra_output']=0
  #Start writing things
  with open(filename,'wb') as f:
    #Each block begins and ends with an integer stating how long each block is
    #(known as FORTRAN binary format)
    #Write the header
    f.write(st.pack('i',256))
    f.write(''.join([st.pack('I',x) for x in header['npart']]))
    f.write(''.join([st.pack('d',x) for x in header['mass']]))
    f.write(st.pack('d',header['time']))
    f.write(st.pack('d',header['redshift']))
    f.write(st.pack('i',header['flag_sfr']))
    f.write(st.pack('i',header['flag_feedback']))
    f.write(''.join([st.pack('I',x) for x in header['nall']]))
    f.write(st.pack('i',header['flag_cooling']))
    f.write(st.pack('i',header['numfiles']))
    f.write(st.pack('d',header['boxsize']))
    f.write(st.pack('d',header['omega0']))
    f.write(st.pack('d',header['omega_lambda']))
    f.write(st.pack('d',header['hubble_param']))
    f.write(st.pack('i',header['flag_age']))
    f.write(st.pack('i',header['flag_metals']))
    f.write(''.join([st.pack('I',x) for x in header['nallhw']]))
    f.write(st.pack('i',header['flag_entr_ics']))
    f.write(st.pack('i',header['extra_output']))
    #extra 56 bytes of nothing
    f.write(''.join([st.pack('i',0) for x in xrange(14)]))
    f.write(st.pack('i',256))
    #Write the position block
    f.write(st.pack('i',pos.shape[0]*12))
    f.write(''.join([st.pack('f',x) for x in pos.flatten()]))
    f.write(st.pack('i',pos.shape[0]*12))
    #write the velocity block
    f.write(st.pack('i',pos.shape[0]*12))
    f.write(''.join([st.pack('f',x) for x in vel.flatten()]))
    f.write(st.pack('i',pos.shape[0]*12))
    #The ids, if given (otherwise make them up)
    if ids is None:
      ids=np.arange(pos.shape[0])
    f.write(st.pack('i',pos.shape[0]*4))
    f.write(''.join([st.pack('I',x) for x in ids]))
    f.write(st.pack('i',pos.shape[0]*4))
    #Write the masses
    f.write(st.pack('i',pos.shape[0]*4))
    f.write(''.join([st.pack('f',x) for x in mass]))
    f.write(st.pack('i',pos.shape[0]*4))
    #If there are any gas particles, write the internal energies
    if header['npart'][0]:
      f.write(st.pack('i',int_energy.shape[0]*4))
      f.write(''.join([st.pack('f',x) for x in int_energy]))
      f.write(st.pack('i',int_energy.shape[0]*4))
      #Write the density
      if rho is not None:
        f.write(st.pack('i',rho.shape[0]*4))
        f.write(''.join([st.pack('f',x) for x in rho]))
        f.write(st.pack('i',rho.shape[0]*4))
      #And the smoothing lengths
      if hsml is not None:
        f.write(st.pack('i',hsml.shape[0]*4))
        f.write(''.join([st.pack('f',x) for x in hsml]))
        f.write(st.pack('i',hsml.shape[0]*4))
    #Any extra quantities to store?
    for extra in extras:
      #What are its dimensions?
      shape=extra.shape
      #What is the type?
      if isinstance(extra[0],int):
        packer = 'i'
      else:
        packer = 'f'
      if len(shape)==2 and shape[1]>1:
        f.write(st.pack('i',shape[0]*shape[1]*4))
        f.write(''.join([st.pack(packer,x) for x in extra.flatten()]))
        f.write(st.pack('i',shape[0]*shape[1]*4))
      else:
        f.write(st.pack('i',shape[0]*4))
        f.write(''.join([st.pack(packer,x) for x in extra]))
        f.write(st.pack('i',shape[0]*4))
    #All done, close the file and finish
    f.close()


#Make loading functions
stint=st.Struct('i')
stuint=st.Struct("I")
stfloat=st.Struct('f')
stdouble=st.Struct('d')

#A bunch of function definitions

def shiftedColorMap(cmap, start=0, midpoint=0.5, stop=1.0, name='shiftedcmap'):
    '''
    Function to offset the "center" of a colormap. Useful for
    data with a negative min and positive max and you want the
    middle of the colormap's dynamic range to be at zero

    Input
    -----
      cmap : The matplotlib colormap to be altered
      start : Offset from lowest point in the colormap's range.
          Defaults to 0.0 (no lower ofset). Should be between
          0.0 and `midpoint`.
      midpoint : The new center of the colormap. Defaults to 
          0.5 (no shift). Should be between 0.0 and 1.0. In
          general, this should be  1 - vmax/(vmax + abs(vmin))
          For example if your data range from -15.0 to +5.0 and
          you want the center of the colormap at 0.0, `midpoint`
          should be set to  1 - 5/(5 + 15)) or 0.75
      stop : Offset from highets point in the colormap's range.
          Defaults to 1.0 (no upper ofset). Should be between
          `midpoint` and 1.0.

    From here: http://stackoverflow.com/questions/7404116/defining-the-midpoint-of-a-colormap-in-matplotlib
    '''
    cdict = {
        'red': [],
        'green': [],
        'blue': [],
        'alpha': []
    }

    # regular index to compute the colors
    reg_index = np.linspace(start, stop, 257)

    # shifted index to match the data
    shift_index = np.hstack([
        np.linspace(0.0, midpoint, 128, endpoint=False), 
        np.linspace(midpoint, 1.0, 129, endpoint=True)
    ])

    for ri, si in zip(reg_index, shift_index):
        r, g, b, a = cmap(ri)

        cdict['red'].append((si, r, r))
        cdict['green'].append((si, g, g))
        cdict['blue'].append((si, b, b))
        cdict['alpha'].append((si, a, a))

    newcmap = matplotlib.colors.LinearSegmentedColormap(name, cdict)
    plt.register_cmap(cmap=newcmap)

    return newcmap

def rolling_window(a, window):
  shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
  strides = a.strides + (a.strides[-1],)
  return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)

def smoothed_gradient(x,y=None,window=11,wsd=3,use_spline=False,spline_s=None):
  """
  Given data points x and y, calculates the gradient evaluated in window
  length bins by least squares minimisation in each bin.  If y is not given
  x is assumed to be sequential integers starting from 0.

  Returns sampled_x,gradient
  """
  #Make window odd by enlarging
  window=window+(window%2+1)%2
  n=len(x)
  if y is None:
    y=x
    x=np.arange(n)
  if use_spline:
    spl=UnivariateSpline(x,y,s=spline_s).derivative()
    return (x,spl(x))
  if window==1:
    return (x[:-1],np.diff(y)/np.diff(x))
  #Build weighting function from gaussian with mean window/2+1 and some sd
  gmean=(window/2)
  gsd=(window/2)/(1.0*wsd)
  weights = np.exp(-(np.arange(window)-gmean)**2/(2.0*gsd*gsd))
  wsum=np.sum(weights)
  #Make it into a matrix for calculation
  wmat = np.outer(np.ones(n-window+1),weights)
  #Calculate rolling window for needed quantities
  xsum=np.sum(rolling_window(x,window)*wmat,1)
  ysum=np.sum(rolling_window(y,window)*wmat,1)
  x2sum=np.sum(rolling_window(x*x,window)*wmat,1)
  xysum=np.sum(rolling_window(x*y,window)*wmat,1)
  m=(xysum*wsum-xsum*ysum)/(wsum*x2sum-xsum*xsum)
  c=(ysum*x2sum-xsum*xysum)/(wsum*x2sum-xsum*xsum)
  return (x[(window/2):(-(window/2))],m)

#Dimensionless phi/dphi
def dimless_phi(x,y,z,q):
  phi = -1.0/((1+q)*np.sqrt(x**2+y**2+z**2))
  phi = phi - q /((1+q)* np.sqrt((x+1)**2+y**2+z**2))
  phi = phi - 0.5 * (((q/(1+q))+x)**2 + y**2)
  return phi

def dimless_grad_phi(x,y,z,q):
  a=1.0/((1+q)*(x**2+y**2+z**2)**1.5)
  b=q/((1+q)*((x+1)**2+y**2+z**2)**1.5)
  dpdx = x*a + (x+1)*b - q/(1+q) - x
  dpdy = y*a + y*b -y
  dpdz = z*a + z*b
  return (dpdx,dpdy,dpdz)

#Calculate L1,L2 and L3 and phi1 for dimensionless phi
#Make grid of xs
xs=np.linspace(-2,1/(1+q),10000)
#Calculate phi at points
phis = dimless_phi(xs,0,0,q)
#Find maximum between stars
o=np.where(np.logical_and(xs>-1,xs<0))[0]
phi1=np.max(phis[o])
L1=xs[o[np.argmax(phis[o])]]
#Where are the zeros?
#First away from the primary
o=np.where(xs>0)[0]
RL3=xs[o[np.argmin(np.abs(phis[o]-phi1))]]
#And outside the secondary
o=np.where(xs<-1)[0]
i=o[-1]
while phis[i]-phi1<0: i=i-1
RL2=xs[i]
#Find L2/L3
tmp=np.linspace(-2,RL2,1000)
px,py,pz=dimless_grad_phi(tmp,0,0,q)
L2=tmp[np.argmin(np.log(px))]
phi2=dimless_phi(L2,0,0,q)
tmp=np.linspace(RL3,2,1000)
px,py,pz=dimless_grad_phi(tmp,0,0,q)
L3=tmp[np.argmin(np.log(px))]
phi3=dimless_phi(L3,0,0,q)

#Find l4/l5 for completness
tmp=np.linspace(0,2,100000)
L4=tmp[np.argmax(dimless_phi(-.5,tmp,0,q))]
L5=-L4
phi4=dimless_phi(-.5,L4,0,q)
phi5=dimless_phi(-.5,L5,0,q)
#Limits on y, by looking around primary
ys=xs
phis=dimless_phi(0,ys,0,q)
o=np.where(ys>0)[0]
y3 = np.abs(ys[o[np.argmin(np.abs(phis[o]-phi1))]])
#And around the secondary
phis=dimless_phi(-1,ys,0,q)
o=np.where(ys>0)[0]
y4 = np.abs(ys[o[np.argmin(np.abs(phis[o]-phi1))]])

def build_boundary(criteria1,criteria2,res=1000,**kw):
  #Create a boundary grid
  buffer_factor=1.2
  xs,ys = np.meshgrid(np.linspace(RL3*buffer_factor,RL2*buffer_factor,res),np.linspace(-y3*buffer_factor,y3*buffer_factor,res))
  grid_phi = dimless_phi(xs,ys,0,q)
  ##Identify boundaries
  grid_marks = np.zeros(xs.shape,dtype=int)
  grid_marks[criteria1(grid_phi,xs,ys,**kw)]=1            
  grid_marks[criteria2(grid_phi,xs,ys,**kw)]=2            
  ##Calculate gradients to find boundaries
  gg=np.gradient(grid_marks)
  gg=np.sqrt(gg[0]**2+gg[1]**2)
  b1=np.where((gg!=0) & (grid_marks==1))
  b2=np.where((gg!=0) & (grid_marks==2))
  lobe1=np.c_[xs[b1],ys[b1]]
  lobe2=np.c_[xs[b2],ys[b2]]
  #Sort them by phi for plotting...
  lobe1=lobe1[np.argsort(np.arctan2(lobe1[:,1],lobe1[:,0])),]
  lobe2=lobe2[np.argsort(np.arctan2(lobe2[:,1],lobe2[:,0]+1.0)),]
  return (lobe1,lobe2)

#Calculate high resolution roche lobes for plotting
def roche_primary_criteria(grid_phi,xs,ys,**kw):
  return ((grid_phi<phi1) & (xs>L1) & (xs<RL3) & (np.abs(ys)<y3))

def roche_secondary_criteria(grid_phi,xs,ys,**kw):
  return ((grid_phi<phi1) & (xs<L1) & (xs>RL2) & (np.abs(ys)<y4))

def primary_boundary_by_phi(grid_phi,xs,ys,phi_max=phi1,**kw):
  return ((grid_phi<phi_max) & (xs>L1) & (xs<RL3) & (np.abs(ys)<y3))

def secondary_boundary_by_phi(grid_phi,xs,ys,phi_max=phi1,**kw):
  return ((grid_phi<phi_max) & (xs<L1) & (xs>RL2) & (np.abs(ys)<y4))

#We'll almost certainly need the Roche lobes, so pre-calculate those
rlobe1,rlobe2 = build_boundary(roche_primary_criteria,roche_secondary_criteria)

#Other criteria we might use
def circular_disc_1(grid_phi,xs,ys,radius=.25,**kw):
  return ((xs**2+ys**2)<radius**2)

def circular_disc_2(grid_phi,xs,ys,radius=.1,**kw):
  return (((xs+1)**2+ys**2)<radius**2)


def to_corot(pos,vel,primary):
  """
  Translate to co-rotating frame and flip about x-axis so that
  the rotation appears counter-clockwise
  """
  #Flip to counter-clockwise rotation
  newpos=np.zeros(np.shape(pos))
  newvel=np.zeros(np.shape(vel))
  phi = np.arctan2(-primary[1],primary[0])
  #Translate positions
  newpos[:,0]=np.cos(phi)*pos[:,0]-np.sin(phi)*pos[:,1]-a*q/(1+q)
  newpos[:,1]=-np.sin(phi)*pos[:,0]-np.cos(phi)*pos[:,1]
  newpos[:,2]=pos[:,2]
  #Translate velocities
  newvel[:,0]=omega*newpos[:,1]+np.cos(phi)*vel[:,0]-np.sin(phi)*vel[:,1]
  newvel[:,1]=-omega*(newpos[:,0]+a*q/(1+q))-np.sin(phi)*vel[:,0]-np.cos(phi)*vel[:,1]
  newvel[:,2]=vel[:,2]
  return (newpos,newvel)

def norm_to_corot(norm,corot):
  """
  Translates raw output binary to corotating frame and saves it to the file corot
  """
  _,mass,pos,vel,rho,hsml,uint,ids,header,extras = read_gadget_binary(norm,True)
  R = np.sqrt(pos[-2:,0]**2+pos[-2:,1]**2)
  if R[0]>R[1]:
    p1 = -1
    p2 = -2
  else:
    p1 = -2
    p2 = -1
  npos,nvel = to_corot(pos,vel,pos[p1,])
  write_gadget_binary(corot,mass,npos,nvel,rho,hsml,uint,ids,header,extras)


def plot_lobe_overlay(crosses=True,zero_force=True,labels=False,stars=False,com=False,pot=None,ax=None,ms=10):
  """
  Adds an overlay to the axis in ax (uses plt.gca if set to None) showing 
  contours of modified potential, Lagrange points and lines of zero force.
  """
  if ax is None:
    ax=plt.gca()
  #A nice roche lobe contour plot...
  xlims=ax.get_xlim()
  ylims=ax.get_ylim()
  x=np.linspace(xlims[0],xlims[1],1000)
  y=np.linspace(ylims[0],ylims[1],1000)
  x,y=np.meshgrid(x,y)
  phi=dimless_phi(x,y,0,q)
  if pot is None:
    pot=np.array([1.,.94327858,.84424028])
  CS=ax.contour(x,y,phi,phi1*pot,colors='black',linestyles=['solid','dashed','dashdot','dotted'])
  if zero_force:
    px,py,pz=dimless_grad_phi(x,y,0,q)
    o=np.where(np.log10(np.abs(px))<-3)
    tmp=np.c_[x[o],y[o]]
    force0_x_left=tmp[tmp[:,0]<=-1]
    force0_x_left=force0_x_left[np.argsort(np.arctan2(force0_x_left[:,1],force0_x_left[:,0]+1)),]
    force0_x_left=np.r_[force0_x_left,force0_x_left[0:1,]]
    force0_x_cen=tmp[(tmp[:,0]<0) & (tmp[:,0]>-1)]
    force0_x_cen=force0_x_cen[np.argsort(force0_x_cen[:,1]),]
    force0_x_right=tmp[tmp[:,0]>=0]
    force0_x_right=force0_x_right[np.argsort(np.arctan2(force0_x_right[:,1],force0_x_right[:,0])),]
    force0_x_right=np.r_[force0_x_right,force0_x_right[0:1,]]
    o=np.where(np.log10(np.abs(py))<-3)
    force0_y=np.c_[x[o],y[o]]
    #Remove those on the x-axis (this is implied)
    force0_y=force0_y[np.abs(force0_y[:,1])>2*np.min(np.abs(y)),]
    force0_y=force0_y[np.argsort(np.arctan2(force0_y[:,1],force0_y[:,0])),]
    ax.plot(force0_y[:,0],force0_y[:,1],'b')
    ax.plot(plt.xlim(),np.zeros(2),'b')
    ax.plot(force0_x_left[:,0],force0_x_left[:,1],'g')
    ax.plot(force0_x_cen[:,0],force0_x_cen[:,1],'g')
    ax.plot(force0_x_right[:,0],force0_x_right[:,1],'g')
  if crosses:
    ax.plot([L1,L2,L3,-.5,-.5],[0,0,0,L4,L5],'kx',ms=ms,markeredgewidth=4)
  if stars:
    ax.plot(-1,0,'*r',ms=ms)
    ax.plot(0,0,'*g',ms=ms)
  if com:
    ax.plot(-q/(1.+q),0,'.b',ms=ms,markeredgewidth=4)
  if labels:
    ax.text(L1+.03,-.025,'L1')
    ax.text(L2-.10,-.025,'L2')
    ax.text(L3+.03,-.025,'L3')
    ax.text(-.5+.03,L4-.025,'L4')
    ax.text(-.5+.03,L5-.025,'L5')

def dimless_extra_phi(x,y,z,q,p1,p2,cs):
  """
  Modified modified potential to include pressure
  forces from static circum-stellar discs
  """
  phi = dimless_phi(x,y,z,q)
  R1=np.sqrt(x**2+y**2+z**2)
  R2=np.sqrt((x+1)**2+y**2+z**2)
  disc1 = -p1*cs*cs*np.log(R1)
  disc1[R1>y3]=0
  disc2 = -p2*cs*cs*np.log(R2)
  disc2[R2>y4]=0
  phi = phi +disc1+disc2
  return phi
