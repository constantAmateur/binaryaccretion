"""
Follow a designated number of particles backwards through the simulations and record
the path they take.  Also calculate useful quantities like the Jacobi constant
along the path.

Parameters are:
  track_start - When to start tracking backwards from (usually the last snapshot)
  new_track_stop - Don't add any new particles if we're earlier than this
  track_stop - When to stop tracking backwards
  ntrack - Number of unique particles to track in each lobe
  phi_track_cut - Particles to track are selected for being deep in the potential
                  they must therefore have a value of the modified potential of 
                  less than phi_track_cut (1.2*phi1 is a decent value)
  R_stop - Distance from centre of mass that we will stop tracking particles

"""
living=True
#Start at the end
i=track_start
#Array with the ids of the particles we're following
following=np.zeros(ntrack*2,dtype=int)
following.fill(-1)
#Keep track of those that have died
dead=np.zeros(ntrack*2)
dead.fill(False)
#The results of our tracking
results=np.zeros((2*ntrack,track_start,5))
results.fill(np.nan)
#How many have we found that meet our cut-off in primary/secondary?
foundp=0
founds=0
while i>=track_stop and living:
  print i
  mass,pos,npos,vel,nvel,rho,hsml,uint,ids,header,extras,p1,p2 = load_file(i)
  if (foundp<ntrack or founds<ntrack) and i>=new_track_stop:
    #Calculate phi
    phi = dimless_phi(npos[:,0],npos[:,1],0,q)
    #Calculate the marker
    markers = np.zeros(npos.shape[0],dtype=int)
    markers[primary_boundary_by_phi(phi,npos[:,0],npos[:,1],phi_max=phi_track_cut)]=1
    markers[secondary_boundary_by_phi(phi,npos[:,0],npos[:,1],phi_max=phi_track_cut)]=2
    #Any new IDs for me to track?
    #On the primary?
    tmp=ids[markers==1]
    tmp=tmp[(tmp!=ids[p1]) & (tmp!=ids[p2])]
    tmp=tmp[np.logical_not(np.in1d(tmp,following))]
    #Will we over-flow?
    n1=min(ntrack-foundp,len(tmp))
    #Store them
    following[foundp:(foundp+n1)]=tmp[:n1]
    #Update counter
    foundp=foundp+n1
    #On the secondary?
    tmp=ids[markers==2]
    tmp=tmp[(tmp!=ids[p1]) & (tmp!=ids[p2])]
    tmp=tmp[np.logical_not(np.in1d(tmp,following))]
    #Will we over-flow?
    n2=min(ntrack-founds,len(tmp))
    #Store them
    following[(ntrack+founds):(ntrack+founds+n2)]=tmp[:n2]
    #Update counter
    founds=founds+n2
    print "Added %d extra to primary tracking and %d extra to secondary tracking"%(n1,n2)
  fi=match(following,ids)
  #Fill in the nans and skip the dead ones
  o=np.where(np.isnan(fi))
  results[o,track_start-i,:]=np.nan
  #Are there any still alive?
  o=np.where(np.logical_and(np.logical_not(dead),np.isfinite(fi)))[0]
  if len(o)!=0:
    #The conversion vector (get local particle properties)
    rfi=fi[o].astype(int)
    #Position
    results[o,track_start-i,0]=npos[rfi,0]
    results[o,track_start-i,1]=npos[rfi,1]
    #Jacobi constant
    results[o,track_start-i,2]=-M2/np.sqrt((npos[rfi,0]+1)**2+npos[rfi,1]**2+npos[rfi,2]**2)-M1/np.sqrt(npos[rfi,0]**2+npos[rfi,1]**2+npos[rfi,2]**2)-0.5*omega*omega*((npos[rfi,0]+(a*q/(1+q)))**2+npos[rfi,1]**2)+0.5*(nvel[rfi,0]**2+nvel[rfi,1]**2+nvel[rfi,2]**2)+uint[rfi]*np.log(rho[rfi])
    #Dissipation
    results[o,track_start-i,3]=extras[0][rfi]/np.log(rho[rfi])
    #Record of this particle goes in its non-accreting Roche lobe
    phi = dimless_phi(npos[rfi,0],npos[rfi,1],0,q)
    markers = np.zeros(len(rfi),dtype=int)
    markers[primary_boundary_by_phi(phi,npos[rfi,0],npos[rfi,1],phi_max=phi_track_cut)]=1
    markers[secondary_boundary_by_phi(phi,npos[rfi,0],npos[rfi,1],phi_max=phi_track_cut)]=2
    #oo=np.where(np.logical_or((o[0]<ntrack) & (markers==2),(o[0]>=ntrack) & (markers==1)))[0]
    results[o,track_start-i,4]=markers
    #Calculate distance to COM
    R = np.sqrt((npos[rfi,0]+(a*q/(1+q)))**2+npos[rfi,1]**2)
    #Remove any ones that are too far out
    results[o[R>R_stop],track_start-i,:]=np.nan
    dead[o[R>R_stop]]=True
    #Move back in time...
    i=i-1
    living=len(o)-np.sum(R>R_stop)
    print "There are still %d left alive"%living
 

def plot_path(pi,subset=None,xlims=(-2,2),ylims=(-2,2),title=None,**kw):
  """
  Plot the particle path with the Jacobi constant below.
  subset - Should either be None (in which case everything is used) or a slice, 
           where the array being indexed is the path from birth to death.
  xlims/ylims - Limits of the plot.
  title - Title for the figure
  kw - Extra arguments passed to plot_lobe_overlay
  """
  #When do we last track this particle?
  died=np.where(np.logical_not(np.isnan(results[pi,:,0])))[0][0]
  #When was it born? (Remember time decreases with increasing index
  born=died+np.sum(np.logical_not(np.isnan(results[pi,:,0])))
  #Work out the subset to use
  if subset is None or type(subset)!=slice:
    subset=slice(born-died)
  #Extract the data
  x=results[pi,died:born,0][::-1][subset][::-1]
  y=results[pi,died:born,1][::-1][subset][::-1]
  J=results[pi,died:born,2][::-1][subset][::-1]
  t=np.arange(track_start-died,track_start-born,-1)[::-1][subset][::-1]/10.
  fig=plt.figure()
  ax1=plt.subplot2grid((5,5),(0,0),colspan=5,rowspan=4)
  ax1.set_xlim(xlims)
  ax1.set_ylim(ylims)
  ax1.set_xlabel("x")
  ax1.set_ylabel("y")
  points = np.array([x, y]).T.reshape(-1, 1, 2)
  segments = np.concatenate([points[:-1], points[1:]], axis=1)
  lc = LineCollection(segments, cmap=plt.get_cmap('Paired'),norm=plt.Normalize(min(t),max(t)))
  lc.set_array(t)
  lc.set_linewidth(3)
  ax1.add_collection(lc)
  plot_lobe_overlay(ax=ax1,**kw)
  ax2=plt.subplot2grid((5,5),(4,0),colspan=5)
  #Reduce number of y-ticks
  ax2.locator_params(nbins=4,axis='y')
  ax2.plot(t,-np.log10(-J))
  ax2.set_ylabel("$-\log_{10}(-J)$")
  ax2.set_xlabel("$t_{dyn}$")
  fig.colorbar(lc,ax=ax1,label="$t_{dyn}$")
  fig.tight_layout()
  plt.draw()

  

#How many of those in the primary, went via the secondary
f1_trans=(np.sum(np.nansum(results[:foundp,:,4]==2,1)!=0))/(1.*foundp)
#How many of those in the secondary, went via the primary
f2_trans=(np.sum(np.nansum(results[ntrack:(ntrack+founds),:,4]==1,1)!=0))/(1.*founds)
#What fraction of accretors went via someone else
ft_trans=(foundp*f1_trans+founds*f2_trans)/(foundp+founds)
