"""
Run once we're in the directory containing snapshots.  Will ensure
everything is in co-rotating coordinates and initialise the variables
and functions that are specific to this folder

Must be run after init.py

Only parameter that needs to be set in the namespace before calling 
is force_reconvert which does what you think.
"""

#These formats should be fixed
file_format="snapshot_%04d"
output_format="corot_%04d"
listing=os.listdir('.')
#Get the normal snapshot files
flz=[x for x in listing if x[:8]=='snapshot']
flz.sort()
#And get those that are already in the right coordinates
dflz=[x for x in listing if x[:5]=="corot"]
dflz.sort()
#Where do the snapshots end and the restarted files begin?
stats_start=int(flz[0][flz[0].rfind("_")+1:])
stats_stop=int(flz[-1][flz[-1].rfind("_")+1:])
#And need a list of the corot numbers already done
didxs=[int(x[x.rfind("_")+1:]) for x in dflz]
#Create the loading function
def load_file(i,process_new=True,write_to_file=True):
  """
  Will load in the binary information into arrays and convert to 
  the co-rotating coordinate system if needed.  Saves to corotating file
  if it isn't already there.
  """
  try:
    _,mass,pos,vel,rho,hsml,uint,ids,header,extras = read_gadget_binary(output_format%i,True)
  except:
    _,mass,pos,vel,rho,hsml,uint,ids,header,extras = read_gadget_binary(file_format%i,True)
  #R
  R = np.sqrt(pos[-2:,0]**2+pos[-2:,1]**2)
  if R[0]>R[1]:
    p1 = -1
    p2 = -2
  else:
    p1 = -2
    p2 = -1
  #Ensure we're in the right frame
  #If primary is at origin, we've already changed frames
  convert=False if R[p1]<1e-3 else True
  if convert:
    npos,nvel = to_corot(pos,vel,pos[p1,])
  else:
    npos,nvel = pos,vel
  if convert and write_to_file:
    write_gadget_binary(output_format%i,mass,npos,nvel,rho,hsml,uint,ids,header,extras)
  return mass,pos,npos,vel,nvel,rho,hsml,uint,ids,header,extras,p1,p2

#Check the snapshots and convert those that need it
for i in xrange(len(flz)):
  j=int(flz[i][flz[i].rfind("_")+1:])
  if j not in didxs or force_reconvert:
    print "Converting snapshot_%d into corot_%d"%(j,j)
    _=load_file(j)
