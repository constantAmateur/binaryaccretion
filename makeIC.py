#!/usr/bin/python
import numpy as np
import h5py as h
import struct as st
import sys,os,argparse
pi=np.pi

parser = argparse.ArgumentParser(description="Make a binary with some gas about to accrete onto it.")
parser.add_argument("filename",type=str,help="""Where to put the resulting GADGET2 binary file.""")
parser.add_argument("j_inf",type=float,help="Angular momentum at infinity",default=1.2)
parser.add_argument("cs",type=float,help="Sound speed/orbital speed",default=.05)
parser.add_argument("q",type=float,help="Mass ratio",default=.2)
parser.add_argument("R",type=float,help="Injection radius",default=20)
parser.add_argument("ndot",type=float,help="Particle injection rate",default=2000)
parser.add_argument("f",type=float,help="Fraction of maximum centripetal acceleration [0-1]",default=1.0)
parser.add_argument("g",type=float,help="Efficiency factor for converting potential to radial velocity [0-1]",default=1.0)
parser.add_argument("-t",action="store_true",help="Make it two dimensional")
parser.add_argument("-nb",action="store_true",help="Remove the binary, concentrate all mass in centre")

args = parser.parse_args(sys.argv[1:])

############################
######   PARAMETERS   ######
############################


#The unit system, defined so G=1
G=1.0
G_SI=6.674e-11
#1AU in metres
L=149597870700.0
#Solar mass in KG
M=1.98855e30
#Which gives a time unit of roughly 1year/2pi
t=np.sqrt(L**3/(G_SI*M))
#Mass of proton in kg
mp=1.672621e-27
#Convert to code units
mp = mp /M
#Boltzman's constant in SI units
kb = 1.3806488e-23
kb = (kb *t*t)/(M*L*L)
#Average molecular weight of gas in proton masses (assume neutral gas = 1.2195)
mu = 1.2195
#Sound speed constant kb/mu*mh
cs_fact = (kb/(mu*mp))

#Numerical things
#How many particles to start with (i.e., how many in this IC)
Nstart = 20
#What radial range to spread over (fraction of R_in)
dR=.01


#Set scale (irrelevant really)
#Binary separation
a = 1.0
#Mass of binary
Mt = 1.0
#Mass accretion rate (per dynamical time)
Mdot = 1e-10

############################
########   SETUP    ########
############################

#Derive things we need from dimensionless numbers
#Temperature of gas
#T = args.cs**2 * (G*Mt/a/cs_fact)
#Injection radius
R_in = args.R*a
#Particle mass
m = Mdot/args.ndot
#Masses and positions
M1 = Mt/(1+args.q)
M2 = args.q*M1
R1 = a*(M2/Mt)
R2 = a*(M1/Mt)
#Calculate the specific relative angular momentum of the binary (the total angular momentum about the centre of mass divided by the reduced mass m1m2/(m1+m2))
j_bin = np.sqrt(G*Mt*a)
#So the angular momentum of particles is then...
j_part = args.j_inf*j_bin

#As the velocity required for the same angular momentum increases as theta decreases, the bounds on theta are
#set such that the orbital speed must be less than  f * v_circ where f is some factor (e.g. .5) and v_circ
#is the velocity needed for a circular orbit
#Require the kinetic energy be a tenth of the 
if args.t: 
  theta_min = pi/2
  theta_max = pi/2
else:
  theta_min = np.arcsin(np.sqrt(args.j_inf/np.sqrt(args.f*args.R)))
  theta_max = pi-theta_min

#Randomly pick them some coordinates
rs = R_in-np.random.rand(Nstart)*dR*R_in
phis = np.random.rand(Nstart)*2*pi
#To ensure roughly uniform sampling of sphere
thetas = np.arccos(np.cos(theta_min)*(np.random.rand(Nstart)*2-1))
#thetas = np.repeat(np.pi*0.5,N)
#thetas = (np.random.rand(N)*(theta_max-theta_min))+theta_min
#Now work out what their speeds should be
#The power of 2 is to convert from physical speed (m/s) to angular speed
vphis = -j_part / (rs*np.sin(thetas))**2
vrs = -args.g * np.sqrt((2*G*Mt/rs)-(j_part/rs/np.sin(thetas))**2)
vthetas = np.zeros(Nstart)

#Convert to cartesian coordinates as these are required for GADGET
xs = rs*np.sin(thetas)*np.cos(phis)
ys = rs*np.sin(thetas)*np.sin(phis)
zs = rs*np.cos(thetas)

vxs = vrs * np.cos(phis) * np.sin(thetas) - ys * vphis
vys = vrs * np.sin(phis) * np.sin(thetas) + xs * vphis
vzs = vrs * np.cos(thetas)

if args.t:
  zs = np.zeros(len(xs))
  vzs = np.zeros(len(vzs))
#Masses
masses = np.repeat(m,Nstart)

#When isothermal equation of state is used, GADGET2 actually wants sound speeds

uint = np.repeat(args.cs*args.cs*(G*Mt/a),Nstart)
#uint = np.repeat((kb*10)/(mu*mp),Nstart)

#Add the binaries to the end
if args.nb:
  xs = np.r_[xs,0]
  ys = np.r_[ys,0]
  zs = np.r_[zs,0]
  vxs = np.r_[vxs,0]
  vys = np.r_[vys,0]
  vzs = np.r_[vzs,0]
  masses = np.r_[masses,M1+M2]
  nstar = 1
else:
  xs = np.r_[xs,0,0]
  ys = np.r_[ys,R1,-R2]
  zs = np.r_[zs,0,0]
  vxs = np.r_[vxs,np.sqrt(G*M2*R1/(a*a)),-np.sqrt(G*M1*R2/(a*a))]
  vys = np.r_[vys,0,0]
  vzs = np.r_[vzs,0,0]
  masses = np.r_[masses,M1,M2]
  nstar = 2

print "%d particles placed at R = %g, theta = [%g,%g]"%(Nstart,R_in,theta_min,theta_max)
print "Particle mass set to %g for %d particles and %g solar masses injected per dynamical time"%(m,args.ndot,Mdot)
print "Efficiency factor for radial velocity set to %g"%args.g
print "Maximum allowed fraction of upper limit on tangential velocity set to %g"%args.f
print "Set length is %g in cm."%(L*100)
print "Set time unit to %g in s."%t
print "Set velocity unit to %g cm/s."%(L*100/t)
print "Set mass unit to %g in grams."%(M*1000)
print "IMPORTANT: Set dNdt in gadget to %g, which could be different from the %g you expect!"%(args.ndot*np.sqrt(G*Mt/a**3),args.ndot)

#Now save the blasted file in the stupid gadget binary format



##Injection angular momentum
#j_inf=args.j_inf
##Gas temperature
#T = 12000
##Binary separation 
#R = 1.0
##Binary mass ratio
#q = .2
##Mass of binary
#Mt = 1.0
##Injection radius in number of outer radii
#r = 20 * R * (1/(1 + q))
##How many particles to start with
#Nstart = 2000
##Particle mass
#m = 5e-14
##The slow drift speed we give to dead gas particles (really purely numeric parameter)
#rdot = .35
#
##!!ALL OTHER PARAMETERS ARE DEPRECATED!!
##What mass accretion rate do you want
#Mdot = 1e-6
##How long do we want to run for?  This is 100 orbits
#time = 100000
#target_time = 1750000
##What resolution do you want (particles per unit mass)
#res = 200000
#############################
#########   SETUP    ########
#############################
#
##Derive needed quantities
#
#M1 = Mt/(1+q)
#M2 = q*M1
#R1 = R*(M2/Mt)
#R2 = R*(M1/Mt)
#
#
##Calculate the specific relative angular momentum of the binary (the total angular momentum about the centre of mass divided by the reduced mass m1m2/(m1+m2))
#j_bin = np.sqrt(G*Mt*R)
#j_part = j_inf*j_bin
##As the velocity required for the same angular momentum increases as theta decreases, the bounds on theta are
##set such that the orbital speed must be less than  f * v_circ where f is some factor (e.g. .5) and v_circ
##is the velocity needed for a circular orbit
#r_min = r
#r_max = r
##Require the kinetic energy be a tenth of the 
#if args.t: 
#  theta_min = 0
#  theta_max = 0
#else:
#  theta_min = np.arcsin(j_inf*np.sqrt(R/(0.2*r_min)))
#  theta_max = pi-theta_min
##How many particles do we need in total
#N = int(np.ceil(Mdot*time*res))
#N = Nstart
##Particle mass
##m=(Mdot*time)/N
##Randomly pick them some coordinates
#rs = (np.random.rand(N)*(r_max-r_min))+r_min
#phis = np.random.rand(N)*2*pi
##To ensure roughly uniform sampling of sphere
#thetas = np.arccos(np.cos(theta_min)*(np.random.rand(N)*2-1))
##thetas = np.repeat(np.pi*0.5,N)
##thetas = (np.random.rand(N)*(theta_max-theta_min))+theta_min
##Now work out what their speeds should be
#vphis = -j_part / (rs*np.sin(thetas))**2
#vrs = np.repeat(-rdot,N)
#vthetas = np.zeros(N)
#
##Convert to cartesian coordinates as these are required for GADGET
#xs = rs*np.sin(thetas)*np.cos(phis)
#ys = rs*np.sin(thetas)*np.sin(phis)
#zs = rs*np.cos(thetas)
#
##vxs = (xs/rs)*vrs + ((xs*zs)/(rs*rs*np.sin(thetas)))*vthetas - (ys/(r*np.sin(thetas)))*vphis
##vys = (ys/rs)*vrs + ((ys*zs)/(rs*rs*np.sin(thetas)))*vthetas + (xs/(r*np.sin(thetas)))*vphis
##vzs = (zs/rs)*vrs - np.sin(thetas) * vthetas
##vxs = (xs/rs)*vrs + ((zs*xs)/(rs*np.sin(thetas)))*vthetas - ys *vphis
##vys = (ys/rs)*vrs + ((zs*ys)/(rs*np.sin(thetas)))*vthetas + xs *vphis
##vzs = (zs/rs)*vrs - (rs*np.sin(thetas))*vthetas
#vxs = vrs * np.cos(phis) * np.sin(thetas) - ys * vphis
#vys = vrs * np.sin(phis) * np.sin(thetas) + xs * vphis
#vzs = vrs * np.cos(thetas)
#
##To test
##N=10000
##xs = xs[:N]
##ys = ys[:N]
##zs = zs[:N]
##vxs = vxs[:N]
##vys = vys[:N]
##vzs = vzs[:N]
#
#
#masses = np.repeat(m,N)
#
##Calculate the internal energy (intended for use with isothermal equation of state flag)
#
#uint = np.repeat((kb*T)/(mu*mp),N)
#
##Add the binaries to the end
#
#xs = np.r_[xs,0,0]
#ys = np.r_[ys,R1,-R2]
#zs = np.r_[zs,0,0]
#
#vxs = np.r_[vxs,np.sqrt(G*M2*R1/(R*R)),-np.sqrt(G*M1*R2/(R*R))]
#vys = np.r_[vys,0,0]
#vzs = np.r_[vzs,0,0]
#
#masses = np.r_[masses,M1,M2]
#
#print "%d particles placed from R = [%g,%g], theta = [%g,%g]"%(N,r_min,r_max,theta_min,theta_max)
#print "Particle mass set to %g"%m
#print "Doom radius should be set to %g."%r
#print "Drift speed = %g"%rdot
#print "Mdot = %g"%Mdot
#print "Injected particles = %d"%N
#print "j_part = %g"%j_part
#print "Set length is %g in cm."%(L*100)
#print "Set time unit to %g in s."%t
#print "Set velocity unit to %g cm/s."%(L*100/t)
#print "Set mass unit to %g in grams."%(M*1000)
##Now save the blasted file in the stupid gadget binary format

############################
#####      IO       ########
############################

#A basic write out to binary routine
with open(args.filename,'wb') as f:
  #Write the header
  f.write(st.pack('i',256))
  npart=[Nstart,nstar,0,0,0,0]
  f.write(''.join([st.pack('I',x) for x in npart]))
  mass=np.zeros(6)
  f.write(''.join([st.pack('d',x) for x in mass]))
  time=0
  f.write(st.pack('d',time))
  redshift=0
  f.write(st.pack('d',redshift))
  flag_sfr=0
  f.write(st.pack('i',flag_sfr))
  flag_feedback=0
  f.write(st.pack('i',flag_feedback))
  nall=npart
  f.write(''.join([st.pack('I',x) for x in nall]))
  flag_cooling=0
  f.write(st.pack('i',flag_cooling))
  numfiles=1
  f.write(st.pack('i',numfiles))
  boxsize=0
  f.write(st.pack('d',boxsize))
  omega0=0
  f.write(st.pack('d',omega0))
  omega_lambda=0
  f.write(st.pack('d',omega_lambda))
  hubble_param=0
  f.write(st.pack('d',hubble_param))
  flag_age=0
  f.write(st.pack('i',flag_age))
  flag_metals=0
  f.write(st.pack('i',flag_metals))
  nallhw=np.zeros(6)
  f.write(''.join([st.pack('I',x) for x in nallhw]))
  flag_entr_ics=0
  f.write(st.pack('i',flag_entr_ics))
  extra_output=0
  f.write(st.pack('i',extra_output))
  #extra 56 bytes of nothing
  f.write(''.join([st.pack('i',0) for x in xrange(14)]))
  f.write(st.pack('i',256))
  #Now we write the blocks...
  #first the position block
  pos=np.c_[xs,ys,zs]
  pos=pos.flatten()
  f.write(st.pack('i',len(pos)*4))
  f.write(''.join([st.pack('f',x) for x in pos]))
  f.write(st.pack('i',len(pos)*4))
  vel = np.c_[vxs,vys,vzs]
  vel = vel.flatten()
  f.write(st.pack('i',len(vel)*4))
  f.write(''.join([st.pack('f',x) for x in vel]))
  f.write(st.pack('i',len(vel)*4))
  #ID isn't given, create one
  ids=np.arange(Nstart+nstar)
  f.write(st.pack('i',len(ids)*4))
  f.write(''.join([st.pack('I',x) for x in ids]))
  f.write(st.pack('i',len(ids)*4))
  #The masses
  f.write(st.pack('i',len(masses)*4))
  f.write(''.join([st.pack('f',x) for x in masses]))
  f.write(st.pack('i',len(masses)*4))
  #The gas particle blocks, if they're needed
  if npart[0]:
    f.write(st.pack('i',npart[0]*4))
    f.write(''.join([st.pack('f',x) for x in uint]))
    f.write(st.pack('i',npart[0]*4))
  #All done...
  



