#Readme

This repository contains all the files necessary to reproduce the simulations presented in [this paper](http://arxiv.org/abs/1412.3963) and [this paper](http://arxiv.org/abs/1507.01850).  To get started, download this repository (either using git clone or by downloading the zip) and navigate to the repository.

Note that the code in this repository (and the linked repository) are probably not particularly usable "out of the box".  The purpose of this repository is to provide an interested expect details of the analysis and data generation that could not be included in the paper and to provide the tools to reproduce the numerical experiments discussed within.

#Creating initial conditions

Initial conditions are created using makeIC.py.  Detailed help can be obtained by running
`./makeIC.py --help`.  To create the example initial condition files given here run:
```
/makeIC.py -t 2D_cs_.25_j_1.2_q_.2_r_20_Ndot_100.IC 1.2 .25 0.2 20 100 1.0 1.0
/makeIC.py -t 2D_cs_.05_j_1.2_q_.2_r_20_Ndot_100.IC 1.2 .05 0.2 20 100 1.0 1.0
```

The parameters are mostly self explanatory if you've read the paper, except for f and g.  
 - g controls the initial radial velocity given to particles, where 1.0 yields marginally bound particles and 0.0 yields no radial velocity.
 - f is only applicable to 3D calculations and effectively sets the maximum height at which particles can be injected.  

The angular momentum, injection radius, injection rate and efficiency parameters f & g used to create the initial condition file must match the corresponding values of the parameters given in the .param file.

#Compiling the right version of GADGET

All simulations for the paper were created using the modified version of GADGET2 available [here](https://github.com/constantAmateur/gadget2-AccretionDiscs).  The particular version of the code used can be obtained [from github](https://github.com/constantAmateur/gadget2-AccretionDiscs/tree/80d4419620be261281d4571aaba982526be06f8e) or [downloaded as a zip file](https://github.com/constantAmateur/gadget2-AccretionDiscs/archive/80d4419620be261281d4571aaba982526be06f8e.zip).

A Makefile with the appropriate compilation flags has been included.  You may need to change the library and compiler settings to suit your particular machine (see the GADGET2 user guide for help with this).  In short, to build the version of GADGET used in this paper, you need to run something similar to:
```
wget https://github.com/constantAmateur/gadget2-AccretionDiscs/archive/80d4419620be261281d4571aaba982526be06f8e.zip
unzip 80d4419620be261281d4571aaba982526be06f8e.zip
#We only need the source files, move and rename them
mv gadget2-AccretionDiscs-80d4419620be261281d4571aaba982526be06f8e/Gadget2AccretionDiscs/Gadget2/ gadget
#Delete everything else
rm -rf gadget2-AccretionDiscs-80d4419620be261281d4571aaba982526be06f8e
#Copy the Makefile and compile
cp Makefile gadget/
cd gadget
make clean;make -j 8
cp Gadget2 ../
```

#Running simulations

It should now be possible to run the simulations using the example parameter files.  For example,
```
mpirun -np 4 ./Gadget2 2D_cs_.05_j_1.2_q_.2_r_20_Ndot_100_acc_.01.param
```

Some things to consider if you run your own simulations.
 - Because global time-steps are used, the smallest time step has a big impact on the run-time of simulations.  Consequently, the value of the accretion radius can have a large impact on run-time.
 - Memory for all particles to be injected is allocated at the start of the simulation, based on the injection rate and the maximum run-time specified.  It is therefore advisable to not set the end time of the simulation to anything too crazily long.  But also don't set it too short, as it is not easy to adjust after starting a run.

#Post-processing

The scripts used to post-process the results of the simulations and produce the plots in the paper can be found in the Analysis/ folder in the file named final_plots_for_paper.py.  In general, init.py is always run first, we change to the directory containing the data and run sim_specific_init.py to create co-rotating snapshots, finally we run averaging.py to create time-averaged quantities, track accretion rates and so on.